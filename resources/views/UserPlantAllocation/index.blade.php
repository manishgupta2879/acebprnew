@extends('layout.app')
@section('pageTitle')
&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;USER PLANT STAGE ALLOCATION</i>
@endsection
@section('content')
 @include('includes.msg')
 @include('includes.validation_messages')
<div class="col-md-12">

<form class="delete" action="" method="post">
  {{ csrf_field() }} 
  <div class="  table-responsive ">
    <table id="table" class="display  table  table-bordered" >
      <thead>
        <tr>     
          <th class="btn-primary">ID</th>
          <th class="btn-primary">User Name</th>
          <th class="btn-primary">User Type</th>
          <th class="btn-primary">Plant Name</th>
           <th class="btn-primary">Plant Code</th>
          
          <th class="btn-primary">Action</th>
        </tr>
      </thead>
      <tbody>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
<?php static $i=1;?>
	
      @foreach($plantAndStageInfo as $user)
        <tr>
              <td>{{$i++}}</td>
	          <td>{{$user->name}}</td>
	          <td>{{$user->type}}</td>         
	          <td>{{$user->plant_name}}</td>  
	          <td>{{$user->plant_code}}</td>  

		      <td>
				<a class="btn btn-primary EditPlant" href="#" data-toggle="modal" data-user_id="{{$user->user_id}}"data-name="{{$user->name}}"  data-type="{{$user->type}}" data-plant_id="{{$user->plant_id}}" ><i class="fa fa-plus">Assign Plant</i></a>
			<!-- 	<a class="btn btn-danger deletePlant" href="#" data-toggle="modal" data-user_id="{{$user->user_id}}"><i class="fa fa-trash"></i> </a> -->
		    </td>
       </tr>
       @endforeach
     </tbody>
   </table>
 </div>
</form>
</div>
<!-- ========================================== Edit Plant-->
<div class="modal fade" id="EditPlantModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title" id="exampleModalLabel">Plant Allocation To User</h3>

	      </div>
	      <div class="modal-body">
	        <form autocomplete="off" action="{{route('plantAllocation.store')}}" method="post" >
	          {{ csrf_field() }} 
	          <div class="form-group">
	            <div class="row">
	              <label class="control-label col-sm-3" for="name">User Name: <span style="color:#f70b0b;">*</span></label>
	              <div class="col-sm-9">
	              	<input type="hidden" id="user_id" name="user_id" value="">
	               <input type="text" class="form-control"  name="name" id="name" placeholder="Enter Username" >
	               @if($errors->has('name'))
	               <div class="error">{{$errors->first('name')}}</div>
	              @endif
	             </div>
	           </div>
	         </div>

	         <div class="form-group">
	         	<div class="row">
	         		<label class="control-label col-sm-3" for="type">USER TYPE:<span style="color:#f70b0b;">*</span> </label>
	         		<div class="col-sm-9">

                 <input type="text" class="form-control"  value = "PRODUCTION" name="type" id="type" placeholder="Enter Username" readonly>
	         			<!-- <select class="form-control type" required="required" id="type" name="type" >
	         				<option value="">--Select Type--</option>
	         				<option value="VENDOR">VENDOR</option>
	         				<option value="BUYER">BUYER</option>
                  <option value="ADMIN">ADMIN</option>             
	         				<option value="PRODUCTION">PRODUCTION</option>             
	         			</select> -->
	         		</div>
	         	</div>
	         </div>
	         <div class="form-group">
	         	<div class="row">
	         		<label class="control-label col-sm-3" for="type">Plant Name:<span style="color:#f70b0b;">*</span> </label>
	         		<div class="col-sm-9">
	         			<select class="form-control plant_name" required="required" id="plant_name" name="plant_name" >
	         				<option value="">--Select Type--</option> 
	         				@foreach($getPlantInfo as $value)
	         				<option value="{{$value->id}}">{{$value->plant_name}}</option> 
	         				@endforeach
	         			</select>
	         		</div>
	         	</div>
	         </div>
	         <div class="form-group">
	         	<div class="row">
	         		<label class="control-label col-sm-3" for="type"">Plant Stages: <span class="required" style="color:#f70b0b;">*</span></label>
	         		<div class="col-sm-9">
	         			<select class="form-control tags" style="width: 100%; color: black;"  id="stage" name="stage[]" multiple>

	         			</select>                         
	         		</div>
	         	</div>
	         </div>
			 <div class="form-group">
			  <div class="modal-footer">
			    <button type="submit" class="btn btn-primary" id="submitPlant">Update</button>
			  </div>
			</div>
	     </form>
	   </div> 
     </div>
   </div>
</div>

<!-- ============================================ delete Plant-->
<div class="modal fade" id="delete_Plant_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<span class="caption-subject text-danger">Confirmation</span>
			</div> 
			<div class="modal-body" id="info">Are you sure ! You want to delete?</div>
			<div class="modal-footer">
				<form action="{{route('plantMaster.delete')}}" method="post">
					{{csrf_field()}}
					<input type="hidden" name="delete_id" class="delete_id">
					<button type="button" data-dismiss="modal" class="btn btn-success">Cancel</button>
					<button type="submit"  class="btn btn-danger" id="delete"><i class="fa fa-trash"></i> Delete</button>
				</form>
			</div>
		</div>
	</div>
</div>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css">
@endsection



@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>



<script type="text/javascript">
  jQuery(document).ready(function() {
    jQuery(".delete").on("submit", function(){
        return confirm("Do you want to update this password?");
    });
});
     jQuery(document).ready(function(){
       jQuery('.plant_code').on('keyup',function(){
      	var plant_code= jQuery('#plant_code').val();
       	  jQuery.ajax({
            type: "get",
            url: "{{route('plantMaster.checkPlantCode')}}",
            data: {plant_code:plant_code},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                  jQuery('#submitPlant').prop('disabled', false);
                  jQuery(".label-important").remove();
                   
                }else{
                   jQuery("#append").append("<span class='label label-important' style='color:red;'>"+"PlantCode Already Registered "+'</span>');
                  jQuery('#submitPlant').prop('disabled', true);
                }

            },
          });
       });

        
             jQuery('.EditPlant').on('click',function(){
             	var name=jQuery(this).data('name');
             	var user_id=jQuery(this).data('user_id');
             	var user_type=jQuery(this).data('type');
             	var plant_code=jQuery(this).data('plant_code');
             	 
             	jQuery('#user_id').val(user_id);
             	jQuery('#name').val(name);
             	jQuery('.type').val(user_type);
             	
             	jQuery('#EditPlantModal').modal('show');
             });

                
	            jQuery('.deletePlant').on('click',function () {
		    	var delete_id = jQuery(this).data('plant_id');
		        jQuery('.delete_id').val(delete_id);
		        jQuery('#delete_Plant_modal').modal('show');
	   

	});
	  
});
jQuery(document).ready(function() {

jQuery('#stage').select2({
        tags: true,
        tokenSeparators: [','],
        placeholder: "Select stage",
       
      });;

});

jQuery(document).ready(function() {
jQuery('.Tags').select2({
        Tags: true,
        tokenSeparators: [','],
        placeholder: "Select stage "
      });

}); 
    
    jQuery(document).ready(function() {
jQuery("#plant_name").on("change",function(event){
    event.preventDefault();
    jQuery("#stage").html("");
    var plant_id = jQuery(this).val();

    jQuery.ajax({
          url:"{{route('plantAllocation.getPlantStages')}}",
          type:"get",
          data: {plant_id:plant_id},
          dataType:'json',
          success: function(response){
             
              if(response.success){

                //jQuery("#stage").html('<option value="">---Select---</option>');
                if(response.data){
                  var option ='';
                  for(var c=0; c <response.data.length; c++){
                  option +="<option style = 'text-color:black' value='"+response.data[c].id+"'>"+response.data[c].stage+"</option>";
    }
                }
                 jQuery("#stage").append(option);
              }
              else{ 
                if(response.message){
                   ViewHelpers.notify("error",response.message);
                } 
              }
          },
          error: function(err){
              //alert(err) ;
          }
      });
    
  });
});	
    
</script>
@endsection