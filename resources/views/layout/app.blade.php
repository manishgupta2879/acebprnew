<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<!--bootstrap default css-->
<link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- CSS-->
<!-- <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}"> -->
<link href="{{ asset('css/skins/_all-skins.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/skins/_all-skins.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css " />
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" href="{{{ asset('images/image.png') }}}">
@yield('header')
<title>ACE MAXIMIZER </title>
</head>
<body class="sidebar-mini fixed"> 
  <div class="wrapper">
     @include('layout.header')

     @include('layout.left-sidebar')
     
      <div class="content-wrapper">
      	@include('layout.breadcrumb')
      	
        @yield('content')
      </div>

     @include('layout.footer')
  </div>
    <!-- Javascripts-->
    <script src="{{ asset('js/jquery-2.1.4.min.js')}}"></script>
	  <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
	  <script src="{{ asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/plugins/pace.min.js')}}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
    
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>


<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js" type="text/javascript"></script>

 <script>       
        $(document).ready(function() {
            $('.search-table').DataTable({
              "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]]
            });
        });
    $(document).ready(function() {
            $('.search').DataTable({
              
            });
        });

    jQuery(document).ready(function(){
    $('#table').DataTable();  
  });
    
    </script>


	@yield('script')
</body>
</html> 	