  <div class="skin-blue">
    <header class="main-header hidden-print"><a class="logo" href=""><img src="{{URL::to('/')}}/images/ace-image.png" alt="ACE" title="ACE" width="103px" style="margin-top: -3px;
    margin-left: -15px;
    width: 150px;
    height: 45px;"></a>
      <nav class="navbar navbar-static-top">  
        <!-- Sidebar toggle button-->
        <a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a> 
        <div class="header-top text-center "></div>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user fa-lg" ></i>
                  <span>{{Auth::user()->name}}</span>
                  (<span>{{Auth::user()->login_id}}</span>)
                </a>
                <li>
                  <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();" ><i class="fa fa-sign-out fa-lg" ></i> LOGOUT</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </li>
            </ul>
          </div>
      </nav>
    </header>
  </div>