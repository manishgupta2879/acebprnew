@extends('layout.app')
@section('pageTitle')

&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;UOM</i>
   
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  

<style type="text/css">
.table >tbody > tr > td
  {
    padding: 1px;
        border: 0px;
  }
  .buttons-html5{
    background-color: #f37c04!important;
        border-color: #f37c04!important;
    position: relative !important;
    top: 0px !important;
        border-radius: 3px;
            box-shadow: none;
    border: 1px solid transparent;
        display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    float: right;
  }
  #example_length{
  margin-bottom: -33px;
}
.input-sm{
  margin-right: 25px;
}
</style>
<div class="col-md-12" style="text-align:center;">
  <div  style="">
      <center>
            @if(Session::has('success'))
                <font style="color:green">{!!session('success')!!}</font>
            @elseif(Session::has('Danger'))
                  <font style="color:red">{!!session('Danger')!!}</font>
            @endif
      </center>
    </div> 
</div>
 <!-- <div class="col-md-12"><br> -->



 <div class="col-md-12">
 <button type="button" style="padding: 7px 12px;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
 <span class="glyphicon glyphicon-plus"></span>
</button>
   <br>
   <br>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">ADD UOM</h3>

      </div>
      <div class="modal-body">
        <form autocomplete="off" action="{{ route('uom.create') }}" method="post" >
        {{ csrf_field() }}
                          
                    
                          <div class="form-group">
                            <div class="row">
                          <label class="control-label col-sm-3" for="name">UOM NAME: <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9" id="append">
                               <input type="text" class="form-control" onkeyup="duplicateuom(this)" required="required" name="name" id="name" placeholder="Uom Name">
                          </div>

                          </div>
                        </div>
                       

                        <div class="form-group">
                          <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="submituser">Submit</button>
      </div>
                        </div>
     
        </form>
      </div>
      
    </div>
  </div>
</div>

    <form class="delete" action="{{ route('uom.update') }}" method="post">
                      {{ csrf_field() }} 
<div class="  table-responsive ">
    <table id="" class="display  table  table-bordered"  style="    width: 50%;">
        <thead>
            <tr>     
                <th class="btn-primary"  >UOM </th>
                <th class="btn-primary"  style="text-align: center;">UPDATE</th>
              
            </tr>
        </thead>
        <tbody>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                               
                      
            @foreach($Uom as $data)
                      
            <tr>
              <input type="hidden" name="update_id[]" value="{{$data->id}}">
                
               
                
                <td style="text-align: center;"><input align="center" class="form-control" type="text"  id="name{{$data->id}}" name="name[]" value="{{$data->name}}"></td>
                <td  style="text-align: center;">
                      <button type="submit" class="btn btn-primary delete"  >
                        <span class="glyphicon glyphicon-edit"></span>
                      </button>
                    
                </td>
            </tr>
            @endforeach
          
        </tbody>
    </table>
</div>
  </form>
</div>

</form>


<script>       

//   jQuery(function(){
//   jQuery('input[type=text]').keyup(function() {
//     var v = jQuery(this).val();
//     var u = v.toUpperCase();
//     if( v != u ) jQuery(this).val(u);
//   });
// });
        $(document).ready(function() {
                  $('#example').DataTable({
              
                    buttons: [
                        'csv'
                        ],
                       
                        "aaSorting": [],
                    dom: 'Blfrtip',
                    "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]]
                  });


              var table = $('#example').DataTable();
                $("#list_filter input").on('keyup click', function() {
                    table.columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).search($(this).val()).draw();
                });
                 
                $("#number_search").on('keyup click', function() {
                    table.column(10).search($(this).val()).draw();
                });
        } );


    </script>


<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $(".delete").on("submit", function(){
        return confirm("Do you want to update this UOM name?");
    });
});
    function duplicateuom(element){
        var name = $(element).val();
        $.ajax({
            type: "get",
            url: "{{url('checkuom')}}",
            data: {names:name},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                  $('#submituser').prop('disabled', false);
                  $(".label-important").remove();
                   
                }else{
                   $("#append").append("<span class='label label-important' style='color:red;'>"+"Uom Already Inserted"+'</span>');
                  $('#submituser').prop('disabled', true);
                }

            },
           
        });
    }
  
</script>

@endsection
