@extends('layout.app')

@section('pageTitle')
&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;STAGE CREATION</i>
@endsection

@section('content')
@include('includes.msg')
@include('includes.validation_messages')

<div class="col-md-12">
	<div class="card" style="background: #f5f5f5;">

		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#STAGE">
		Add Plant Stage&nbsp;</a>

		<div class="modal fade" id="STAGE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button btn-primary" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"class="text-danger">x</span></button>
						<h5 class="modal-title "><i class="fa fa-plus"></i>&nbsp;Add Plant Stage</h5>
					</div>
					<div class="modal-body">
						<div class="form portlet-body ">
							<form action="{{route('plantStageMaster.store')}}" method="post">
								{{csrf_field()}}
								<div class="form-group is-empty">
									<div class="row">
										<input type="hidden" name="auth" class="auth" value="{{(Auth::user()->type)}}">

										<div class="col-md-12 col-sm-12" style="margin-bottom: 10px;">
											<select class="form-control"  name="plant_name" class="plant_name">

												@foreach($plantInfo as $data)
												<option value="{{$data->id}}">{{$data->plant_name}}</option>
												@endforeach
											</select>
										</div>

										<div class="col-md-12 col-sm-12">
											<input typet="text" class="form-control" name="stage" placeholder="Enter Plant Stages Name" required="">
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-12" style="text-align: center;">
											<button type="submit" class="demo-loading btn btn-primary" id="STAGEMasters"><i class="fa fa-check">&nbsp;Submit</i></button>
										</div>
									</div>
								</div>
							</form>
						</div>									
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div id="content">
			<div class="portlet box blue">
				<div class="portlet-title">

					<div class="clearfix"></div>
					<div class="tools">	</div>
				</div>
			</div>
		</div>
		<div class="portlet-body">

			<div class="modal fade" id="EditStageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="text-danger">×</span></button>
							<h5 class="modal-title"><i class="fa fa-edit"></i> Edit Plant Stages</h5>
						</div>
						<div class="modal-body">
							<div class="form portlet-body ">
								<form action="{{route('plantStageMaster.update')}}" method="post">
									<div class="form-group is-empty">
										{{csrf_field()}}

										<div class="form-group">
											<div class="row">
												<input type="hidden" name="updateStage_id" class="updateStage_id" id="updateStage_id">

												<label class="control-label col-sm-3" for="login_id">Plant Name:<span style="color:#f70b0b;">*</span> </label>
												<div class="col-sm-9" >
													<select class="form-control"  name="plant_name" class="plant_name">
														@foreach($plantInfo as $data)
														<option value="{{$data->id}}">{{$data->plant_name}}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">												
												<label class="control-label col-sm-3" for="login_id">Stage:<span style="color:#f70b0b;">*</span> </label>
												<div class="col-sm-9" >
													<input typet="text" class="form-control col-sm-9 stage" id="stage" name="stage" placeholder="Enter stage Name" required="">
													<span id="stage_typeerr"></span>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label class="control-label col-sm-3" for="login_id">Status:<span style="color:#f70b0b;">*</span> </label>
												<div class="col-sm-9" id="append">
													<select name="is_active" id="IS_ACTIVE" class="form-control">
														<option value="1">Active</option>
														<option value="0">De-Active</option>
													</select>
												</div>
											</div>
										</div>
									</div>									
									<div class="form-actions">
										<div class="row">
											<div class="col-md-12" style="text-align:center;">
												<button type="submit" id="update"class="demo-loading-btn btn btn-primary"><i class="fa fa-edit">Update</i></button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="delete_leave_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="text-danger">×</span></button>
						<span class="caption-subject text-danger font-red-sunglo bold uppercase">Confirmation</span>
					</div>
					<div class="modal-body" id="info">Are you sure ! You want to delete?</div>
					<div class="modal-footer">
						<form action="{{route('plantStageMaster.delete')}}" method="post">
							{{csrf_field()}}
							<input type="hidden" name="delete_id" class="delete_id">
							<button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
							<button type="submit"  class="btn btn-danger" id="delete"> Delete</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="table-responsive">
			<table id="table" class="display  table  table-bordered" style="margin: 10px;">
				<thead>
					<tr>
						<th>#ID</th>
						<th>Plant Name</th>
						<th>Stages</th>
						<th>Active</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<input type="hidden" name="" value="{{$i=1}}">	
					@foreach($getPlantInfo as $value)	
					<tr>
						<td>{{$i++}}</td>												
						<td>{{$value->plant_name}}</td>
						<td>{{$value->stage}}</td>
						@if($value->is_active=='1')
						<td>Active</td>
						@else
						<td>De-Active</td>
						@endif
						<td>

							<a class="btn btn-primary edit_Stage"  data-toggle="modal" data-Plant_name="{{$value->plant_name}}" data-stage="{{$value->stage}}" data-stage_id="{{$value->stageId}}"><i class="fa fa-edit"></i></a>
							
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>			
	</div>
</div>




<div class="modal fade" id="ModalleaveMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" 
					class="text-danger">×</span></button>
					<h5 class="modal-title"><i class="fa fa-plus"></i>&nbsp;Add Plant Stage</h5>
				</div>
				<div class="modal-body">
					<div class="form portlet-body ">
						<form action="{{route('plantStageMaster.store')}}" method="post">
							{{csrf_field()}}
							<div class="form-group is-empty">
								<div class="row">
									<input type="hidden" id="auth" name="auth" value="{{(Auth::user()->type)}}">

									<div class="col-md-12 col-sm-12" style="margin-bottom: 10px;">
										<select  name="plant_name" class="plant_name">

											@foreach($plantInfo as $data)
											<option value="{{$data->id}}">{{$data->plant_name}}</option>
											@endforeach
										</select>
									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12" style="text-align: center;">
										<button type="submit" id="model"class="demo-loading-btn btn green"><i class="fa fa-check">&nbsp;Submit</i></button>
									</div>
								</div>
							</div>
						</form>
					</div>									
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
<script type="text/javascript">
	
	jQuery(document).ready(function(){
		jQuery('.deleteStage').on('click',function () {
			var delete_id = jQuery(this).data('stage_id');
			jQuery('.delete_id').val(delete_id);
			jQuery('#delete_leave_modal').modal('show');
		});


		$('.edit_Stage').on('click',function(){
			var stage_id=$(this).data('stage_id');
			var plant_name=$(this).data('plant_name');
			var stage=$(this).data('stage');
			jQuery('#updateStage_id').val(stage_id);
			jQuery('#plant_Name').val(plant_name);
			jQuery('#stage').val(stage);
			jQuery('#EditStageModal').modal('show');
		});
	});

	jQuery(document).ready(function(){
		$('#table').DataTable();	
	});

</script>

@endsection
