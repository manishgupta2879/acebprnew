@extends('layout.app')
@section('pageTitle')
&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;STOCK DATA</i>
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <style type="text/css">
  td{font-size: 12px;}
  .buttons-html5{
    background-color: #f37c04!important;
        border-color: #f37c04!important;
    position: relative !important;
    top: 0px !important;
        border-radius: 3px;
            box-shadow: none;
    border: 1px solid transparent;
        display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    float: right;
  }
  #example_length{
  margin-bottom: -33px;
}
.input-sm{
  margin-right: 25px;
}
</style>
<form action="{{route('stockdetail.oneupdate')}}" method="post" class="delete">
  {{csrf_field()}}
<div class="col-md-12" style="text-align:center;">
   
<button type="submit" class="btn btn-primary" >
 <span class="glyphicon glyphicon-edit"></span>
</button>

</div>
 <div class="col-md-12">

<div class="  table-responsive ">
    <table id="example" class="display  table  table-bordered"  " >
        <thead>
            <tr>     
                <th class="btn-primary">LAST UPDATED</th>
                <th class="btn-primary">ITEM</th>
                <th class="btn-primary">TYPE</th>
                <th class="btn-primary">UOM</th>
                <th class="btn-primary" >NORM</th>
                <th class="btn-primary">FG/STORE STOCK</th>
                <th class="btn-primary">WIP STOCK</th>
                <th style="min-width: 100px;" class="btn-primary">TO PROCURE /PRODUCE</th>
                <th class="btn-primary">REMARK</th>
                <th class="btn-primary">ECO COLOR</th>
               
                
            </tr>
        </thead>
        <tbody>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            @foreach($stock_list as $data)
                      <script type="text/javascript">
           $(document).ready(function(){
            $('#norm{{$data->id}}, #fg_store{{$data->id}}, #wip_stock{{$data->id}}').on('keyup change',function () {debugger;
            
             var norm =  parseFloat($("#norm{{$data->id}}").val());
             var fg_store =  parseFloat($("#fg_store{{$data->id}}").val());
             var wip_stock =  parseFloat($("#wip_stock{{$data->id}}").val());
             if(isNaN(wip_stock)) {
                var wip_stock = 0;
                }
             var procure = (parseFloat(norm)-(parseFloat(fg_store)+parseFloat(wip_stock)));
             var procures = ((parseFloat(fg_store)+parseFloat(wip_stock))/parseFloat(norm)*100);

                if (procure <= 0) {
                    procure=0.00;
                }
                if (isNaN(procure)) {
                    procure=0.00;
                }

                                    if (0.00>=procures) {
                                        $("#color{{$data->id}}").css("background-color","#000");
                                        $("#color{{$data->id}}").css("color","#FFF");
                                        $("#color{{$data->id}}").html("Black");  

                                        $("#colors{{$data->id}}").css("background-color","#000");
                                        $("#colors{{$data->id}}").css("color","#FFF");
                                        $("#colors{{$data->id}}").val("Black");

                                    }
                                 
                                    if (procures>0.00 && procures <= 33.33) {
                                        $("#color{{$data->id}}").css("background-color","#FF0000");
                                        $("#color{{$data->id}}").html("Red");
                                        
                                        $("#colors{{$data->id}}").css("background-color","#FF0000");
                                        $("#colors{{$data->id}}").val("Red");                                        
                                    }
                                   
                                    if (procures>33.33 && procures<=66.66) {
                                        $("#color{{$data->id}}").css("background-color","#FFFF00");
                                        $("#color{{$data->id}}").html("Yellow");

                                        $("#colors{{$data->id}}").css("background-color","#FFFF00");
                                        $("#colors{{$data->id}}").val("Yellow");
                                    }
                                   
                                    if (procures>66.66 && procures<=100) {
                                        $("#color{{$data->id}}").css("background-color","#64FF33");
                                        $("#color{{$data->id}}").html("Green");

                                        $("#colors{{$data->id}}").css("background-color","#64FF33");
                                        $("#colors{{$data->id}}").val("Green");                                        
                                    }
                                    if (procures>100) {                                
                                        $("#color{{$data->id}}").css("background-color","#FFF");
                                        $("#color{{$data->id}}").css("color","#000");
                                        $("#color{{$data->id}}").html("White");

                                        $("#colors{{$data->id}}").css("background-color","#FFF");
                                        $("#colors{{$data->id}}").css("color","#000");
                                        $("#colors{{$data->id}}").val("White");                                        
                                    }

             $("#procure{{$data->id}}").val(procure.toFixed(2));


           });
          });


        </script>
    
            <tr><input type="hidden" name="update_id" value="{{$data->id}}">
                <td >{{Carbon\Carbon::parse($data->updated_date)->format('d/m/Y')}}</td>
                <td>{{$data->item}}</td>
                <td>{{$data->type}}</td>
                <td>{{$data->UOM}}</td>
                <td><input style="max-width: 85px;" type="text" class="numeric" id="norm{{$data->id}}" name="norm" value="{{$data->norm}}"></td>
                <td><input style="max-width: 85px;" type="text" class="numeric" id="fg_store{{$data->id}}" name="fg_store" value="{{$data->fg_store}}"></td>
                <td>@if($data->type=="FG")
                    <input style="max-width: 85px; background-color: #ebebe4;" type="text" class="numeric"  readonly="readonly"  name="wip_stock" value="">
                @else
                <input type="text" style="max-width: 85px;"  class="numeric" id="wip_stock{{$data->id}}" name="wip_stock" value="{{$data->wip_stock}}">
            @endif</td>
                <td><input style="max-width: 85px;background-color: #ebebe4;" type="text"  readonly="readonly" class="numeric" id="procure{{$data->id}}" name="procure" value="{{$data->procure}}"></td>
                <td><textarea style="max-width:111px;" name="remark">{{$data->remark}}</textarea></td>
                   <td id="color{{$data->id}}" @switch($data->color)
                                        @case ("Red")
                                            style="background-color:#FF0000;"
                                            @break
                                        @case ("Green")
                                            style="background-color:#64FF33;"
                                            @break
                                        @case ("Yellow")
                                            style="background-color:#FFFF00;"
                                            @break
                                         @case ("Black")
                                            style="background-color:#000;color:#fff;"
                                            @break
                                        @default
                                            style="background-color:#fff;color:#000"
                                            @break
                                    @endswitch >{{$data->color}} 
                                </td>

                              
                                    <input type="hidden" id="colors{{$data->id}}" name="colors" value="{{$data->color}}">
            </tr>
            </form>
            @endforeach
        </tbody>
    </table>
</div>

</div>


</form>


          



@endsection

    @section('script')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    
   <script>       
        $(document).ready(function() {
            $('#example').DataTable({
              
              buttons: [
                  'csv'
                  ],
                 
                  "aaSorting": [],
              dom: 'Blfrtip',
              "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]]
            });

              var table = $('#example').DataTable();
                $("#list_filter input").on('keyup click', function() {
                    table.columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).search($(this).val()).draw();
                });
                 
                $("#number_search").on('keyup click', function() {
                    table.column(10).search($(this).val()).draw();
                });
        } );
    </script>
    <script type="text/javascript">


        jQuery(function(){
            var vil={
                init:function(){

                    vil.date();
                    console.log('hello');
                },

                date:function(){
                    jQuery(".datepicker").datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        format: 'dd/mm/yyyy'
                    });

                },
                
            }
            vil.init();
        });

                jQuery(document).ready(function()
                    {      

                    $("select").on('change',function(){     

                     var type = $("#type").val();

                     if (type == 'FG') {

                      $('.WIP').hide();  

                     }

                    });           
                    
                    });

                       $('.numeric').keypress(function (event) {
                            var keycode = event.which;
                            if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
                                event.preventDefault();
                            }
                        });

                       $('#texat').keypress(function (e) {
                            var regex = new RegExp("^[a-zA-Z0-9]+$");
                            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                            if (regex.test(str)) {
                                return true;
                            }

                            e.preventDefault();
                            return false;
                        });

                        jQuery(function(){
                              jQuery('input[type=email],input[type=text],textarea').keyup(function() {
                                var v = jQuery(this).val();
                                var u = v.toUpperCase();
                                if( v != u ) jQuery(this).val(u);
                              });
                            });
 $(".delete").on("submit", function(){
        return confirm("Do you want to Update this items?");
    });
  $(document).ready(function() {
    $('#tableName').dataTable( {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    } );
} );
    </script>
    @endsection