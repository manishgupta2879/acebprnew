@extends('layout.app')
@section('pageTitle')

&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;PASSWORD UPDATE</i>
   
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  

<style type="text/css">
.table >tbody > tr > td
  {
    padding: 1px;
  }
  .buttons-html5{
    background-color: #f37c04!important;
        border-color: #f37c04!important;
    position: relative !important;
    top: 0px !important;
        border-radius: 3px;
            box-shadow: none;
    border: 1px solid transparent;
        display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    float: right;
  }
  #example_length{
  margin-bottom: -33px;
}
.input-sm{
  margin-right: 25px;
}
</style>
<div class="col-md-12" style="text-align:center;">
   @include('includes.msg')
 @include('includes.validation_messages')
</div>




<div class="col-md-12">
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="padding: 7px 12px;">
   <span class="glyphicon glyphicon-plus"></span>
 </button>
 <br>
 <br>
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">ADD USER</h3>

      </div>
      <div class="modal-body">
        <form autocomplete="off" action="{{ route('admin.create') }}" method="post" >
          {{ csrf_field() }}
          
          
          <div class="form-group">
            <div class="row">
              <label class="control-label col-sm-3" for="name">NAME: <span style="color:#f70b0b;">*</span></label>
              <div class="col-sm-9">
               <input type="text" class="form-control" required="required" name="name" id="name" >
             </div>

           </div>
         </div>
         
         <div class="form-group">
          <div class="row">
            <label class="control-label col-sm-3" for="login_id">USER ID:<span style="color:#f70b0b;">*</span> </label>
            <div class="col-sm-9" id="append">
             <input type="text" onkeyup="duplicateEmail(this)" class="form-control" required="required" name="login_id" id="login_id" >
           </div>
         </div>
       </div>
       <div class="form-group">
        <div class="row">
          <label class="control-label col-sm-3" for="type">USER TYPE:<span style="color:#f70b0b;">*</span> </label>
          <div class="col-sm-9">
           <select class="form-control" required="required" id="type" name="type" >
             <option value="">--Select Type--</option>
             <option value="VENDOR">VENDOR</option>
             <option value="BUYER">BUYER</option>
             <option value="ADMIN">ADMIN</option>
             <option value="PRODUCTION">PRODUCTION</option>
             
           </select>
         </div>
       </div>
     </div>
      <div class="form-group">
        <div class="row">
          <label class="control-label col-sm-3" for="type">Plant Name:<span style="color:#f70b0b;">*</span> </label>
          <div class="col-sm-9">
           <select class="form-control" required="required" id="plant_name" name="plant_name" >
             <option value="">--Select Type--</option> 
             @foreach($getPlantInfo as $value)
             <option value="{{$value->id}}">{{$value->plant_name}}</option> 
             @endforeach
           </select>
         </div>
       </div>
     </div>
     <div class="form-group">
      <div class="row">
      <label class="control-label col-sm-3" for="type"">Plant Stages: <span class="required" style="color:#f70b0b;">*</span></label>
        <div class="col-sm-9">
          <select class="form-control tags" style="width: 100%; color: black;"  id="stage" name="stage[]" multiple>

          </select>                         
       </div>
    </div>
    </div>
  
     <div class="form-group">
       <div class="row">
        <label class="control-label col-sm-3" for="mobile">MOBILE:<span style="color:#f70b0b;">*</span> </label>
        <div class="col-sm-9">
         <input type="text"  class="form-control " required="" id="mobile" name="mobile">
       </div>
     </div>
   </div>
   <div class="form-group">
     <div class="row">
      <label class="control-label col-sm-3" for="password">PASSWORD: <span style="color:#f70b0b;">*</span></label>
      <div class="col-sm-9">
       <input type="password"  class="form-control" required=""  name="password">
     </div>
   </div>
 </div>
 <div class="form-group">
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary" id="submituser">Submit</button>
  </div>
</div>

</form>
</div>

</div>
</div>
</div>

<for
m class="delete" action="{{ route('admin.update') }}" method="post">
  {{ csrf_field() }} 
  <div class="  table-responsive ">
    <table id="example" class="display  table  table-bordered"  " >
      <thead>
        <tr>     
          <th class="btn-primary">USER NAME</th>
          <th class="btn-primary">USER TYPE</th>
          <th class="btn-primary">LOGIN ID</th>
          <th class="btn-primary">PASSWORD</th>
          <th class="btn-primary">UPDATE</th>
          
          
        </tr>
      </thead>
      <tbody>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        @foreach($users as $data)

        <tr>
          <input type="hidden" name="update_id[]" value="{{$data->id}}">
          
          <td>{{$data->name}}</td>
          <td>{{$data->type}}</td>
          <td>{{$data->email}}</td>
          <td><input style="max-width: 85px;" type="password" id="norm{{$data->id}}" name="password[]" value="{{$data->password}}"></td>

          <td>
            <button type="submit" class="btn btn-primary delete" >
             <span class="glyphicon glyphicon-edit"></span>
           </button>
          
         </td>
       </tr>
       @endforeach
       
     </tbody>
   </table>
 </div>
</form>
</div>
<!-- ============================================================Edit user Model ================================================================ -->


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css">
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script type="text/javascript">
 
      jQuery(document).on('click','#EditUserId', function(){ 

              var update_id=jQuery(this).data('update_id');
             jQuery.ajax({
                url:"{{route('admin.checkStageAndPlant')}}",
                type:"get",
                data:{user_id:update_id},
                dataType:"json",
                success: function(response){
                  console.log(response.data['stageExists']);

              if(response.success){
                if(response.data){
                  
                  
                }
                
              }
              else{ 
                if(response.message){
                   ViewHelpers.notify("error",response.message);
                } 
              }
          },
          error: function(err){
              //alert(err) ;
          }
               

             });
        });
 

</script>

<script type="text/javascript">

  jQuery(document).ready(function(){
      jQuery("#Plant_name").on("change",function(){  
        var Login_id=jQuery("#Login_id").val();   
       jQuery.ajax({
          type:"get",
          url: "{{route('admin.PlantAllocationCheck')}}",
          data:{login_id:Login_id},
          dataType:"json",
          success:function(res){              
                if(res.exists){
                   $('#UserSubmit').prop('disabled',false);
                   $(".label-important").remove();                   
                  }else{
                      $(".label-important").remove("");
                   $("#userPlantAllocation").append("<span class='label label-important' style='color:red;'>"+"Plant  Already Assign To this user !!"+'</span>');
                  $('#UserSubmit').prop('disabled', true);

                }
              
          },
        });
     
      });
    });
 
  $(document).ready(function() {
    $(".delete").on("click", function(){
       
    });
});


jQuery(document).ready(function() {

jQuery('#stage').select2({
        tags: true,
        tokenSeparators: [','],
        placeholder: "Select stage",
       
      });;

});

jQuery(document).ready(function() {
jQuery('.Tags').select2({
        Tags: true,
        tokenSeparators: [','],
        placeholder: "Select stage "
      });

});


 jQuery(document).ready(function() {
    
       $(".editUser").on("click", function(){ 
     
              var name=$(this).data('name');
              var type=$(this).data('type');
              var login_id=$(this).data('login_id');
              var mobile=$(this).data('mobile');
              var update_id=$(this).data('update_id');
              var plant_id=$(this).data('plant_id');
              jQuery('#Name').val(name);
              jQuery('#Update_id').val(update_id);
              jQuery('#Type').val(type);
              jQuery('#Login_id').val(login_id);
              jQuery('#Mobile').val(mobile);
              if((plant_id)!=null)
              jQuery('#Plant_name').val(plant_id);
              jQuery('#editUserModal').modal('show');
             });
    });


 


    function duplicateEmail(element){
        var login_id = $(element).val();
        $.ajax({
            type: "get",
            url: "{{url('checkuserEmail')}}",
            data: {login_ids:login_id},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                  $('#submituser').prop('disabled', false);
                  $(".label-important").remove();
                   
                }else{
                   $("#append").append("<span class='label label-important' style='color:red;'>"+"User Already Registered"+'</span>');
                  $('#submituser').prop('disabled', true);
                }

            },
           
        });
    }



// ============================================================================= for load to satages

jQuery(document).ready(function() {
jQuery("#plant_name").on("change",function(event){
    event.preventDefault();
    jQuery("#stage").html("");
    var plant_id = jQuery(this).val();

    jQuery.ajax({
          url:"{{route('plantAllocation.getPlantStages')}}",
          type:"get",
          data: {plant_id:plant_id},
          dataType:'json',
          success: function(response){
             
              if(response.success){

                //jQuery("#stage").html('<option value="">---Select---</option>');
                if(response.data){
                  var option ='';
                  for(var c=0; c <response.data.length; c++){
                  option +="<option style = 'text-color:black' value='"+response.data[c].id+"'>"+response.data[c].stage+"</option>";
    }
                }
                 jQuery("#stage").append(option);
              }
              else{ 
                if(response.message){
                   ViewHelpers.notify("error",response.message);
                } 
              }
          },
          error: function(err){
              //alert(err) ;
          }
      });
    
  });
});


// =============================================for update user =====================================================
jQuery(document).ready(function() {
jQuery("#Plant_name").on("change",function(event){
    event.preventDefault();
    jQuery("#Stage").html("");
    var plant_id = jQuery(this).val();

    jQuery.ajax({
          url:"{{route('admin.getPlantStages')}}",
          type:"get",
          data: {plant_id:plant_id},
          dataType:'json',
          success: function(response){
             
              if(response.success){

                //jQuery("#stage").html('<option value="">---Select---</option>');
                if(response.data){
                  for(var c=0; c <response.data.length; c++){
    jQuery("#Stage").append('<option style="color:black;" value="'+response.data[c].id+'">'+response.data[c].stage+'</option>');
    }
                }
              }
              else{ 
                if(response.message){
                   ViewHelpers.notify("error",response.message);
                } 
              }
          },
          error: function(err){
              //alert(err) ;
          }
      });
    
  });
});
 
</script>


@endsection
