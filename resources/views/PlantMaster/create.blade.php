@extends('layout.app')
@section('pageTitle')
&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;BPR DATA</i>
@endsection
@section('content')

<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">Plant Master Creation</h3></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8">
						<form id="myform"class="form-horizontal" action="" method="post">
							{{csrf_field()}}
						
							<div class="form-group is-empty">
								<label class="col-md-4 control-label">Plant Name<span style="color: red;" class="required">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="plant_name" value="" placeholder="Enter Plant Name">
								</div>
							</div>
							<div class="form-group is-empty">
								<label class="col-md-4 control-label">Plant Code<span style="color: red;" class="required">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="plant_code" value="" placeholder="Enter Plant Code">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-8 col-md-offset-4">
									<input type="submit" id="submit" class="btn btn-success" style="background: #131257;">
								</div>
							</div>
						</form>
					</div>
					<!-- <div class="col-md-4">
						<div class="panel panel-custom">
							
							<div class="panel-heading" style="border-bottom: 1px solid #131257;">
								<div class="panel-title">
									<strong>My Leave Details</strong>
								</div>
							</div>
							<table class="table">
								<tbody>
									
									<tr>
										<td><strong></strong>:</td>
										<td></td>
										</tr>

										
									</tbody>
								</table>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection



@section('script')



@endsection