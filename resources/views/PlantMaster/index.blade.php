@extends('layout.app')
@section('pageTitle')
&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;PLANT CREATION</i>
@endsection
@section('content')

 @include('includes.msg')
 @include('includes.validation_messages')
<div class="col-md-12">
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#PlantMaster" style="padding: 7px 12px;">
   <span class="glyphicon glyphicon-plus"><strong>Add Plant</strong></span>
 </button>
 <br>
 <br>
 <div class="modal fade" id="PlantMaster" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title" id="exampleModalLabel">Add Plant</h3>

	      </div>
	      <div class="modal-body">
	        <form method="post" action="{{route('plantMaster.store')}}" >
	          {{ csrf_field() }} 
	          <div class="form-group">
	            <div class="row">
	              <label class="control-label col-sm-3" for="name">Plant Name: <span style="color:#f70b0b;">*</span></label>
	              <div class="col-sm-9">
	               <input type="text" class="form-control"  name="plant_name" id="plant_name" placeholder="Enter Plant Name" value="{{old('plant_name')}}" >
	               @if($errors->has('plant_name'))
	               <div class="error">{{$errors->first('plant_name')}}</div>
	              @endif
	             </div>
	           </div>
	         </div>
	         
	         <div class="form-group">
	          <div class="row">
	            <label class="control-label col-sm-3" for="login_id">Plant Code:<span style="color:#f70b0b;">*</span> </label>
	            <div class="col-sm-9" id="append">
	             <input type="text"  class="form-control plant_code" required="required" name="plant_code" id="plant_code" placeholder="Enter Plant Code" value="{{old('plant_code')}}">
	             @if($errors->has('plant_code'))
	             <div class="error">{{$errors->first('plant_code')}}</div>
	             @endif
	           </div>
	         </div>
	       </div>
			 <div class="form-group">
			  <div class="modal-footer">
			    <button type="submit" class="btn btn-primary" id="submitPlant">Submit</button>
			  </div>
			</div>
	     </form>
	   </div> 
     </div>
   </div>
</div>


<form class="delete" action="" method="post">
  {{ csrf_field() }} 
  <div class="  table-responsive ">
    <table id="table" class="display  table  table-bordered" >
      <thead>
        <tr>     
          <th class="btn-primary">ID</th>
          <th class="btn-primary">Plant Name</th>
          <th class="btn-primary">Plant Code</th>
          <th class="btn-primary">Active</th>
          <th class="btn-primary">Action</th>
        </tr>
      </thead>
      <tbody>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        @foreach($plantInfo as $value) 
<?php static $i=1;?>
        <tr>
          
	          <td>{{$i++}}</td>
	          <td>{{$value->plant_name}}</td>
	          <td>{{$value->plant_code}}</td>
	          @if($value->IS_ACTIVE=='1')
	          <td>Active</td>
	          @else
	          <td>De-Active</td>
	          @endif    
		      <td>
				<a class="btn btn-primary EditPlant" href="#" data-toggle="modal" data-Plant_name="{{$value->plant_name}}" data-plant_code="{{$value->plant_code}}" data-plant_id="{{$value->id}}"><i class="fa fa-edit"></i></a>
				<!-- <a class="btn btn-danger deletePlant" href="#" data-toggle="modal" data-plant_id="{{$value->id}}"><i class="fa fa-trash"></i> </a> -->
		    </td>
       </tr>
       @endforeach
     </tbody>
   </table>
 </div>
</form>
</div>
<!-- ========================================== Edit Plant-->
<div class="modal fade" id="EditPlantModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title" id="exampleModalLabel">Edit Plant</h3>

	      </div>
	      <div class="modal-body">
	        <form autocomplete="off" action="{{ route('plantMaster.update') }}" method="post" >
	          {{ csrf_field() }} 
	          <div class="form-group">
	            <div class="row">
	              <label class="control-label col-sm-3" for="name">Plant Name: <span style="color:#f70b0b;">*</span></label>
	              <div class="col-sm-9">
	               <input type="text" class="form-control"  name="plant_name" id="plant_Name" placeholder="Enter Plant Name" >
	               @if($errors->has('plant_name'))
	               <div class="error">{{$errors->first('plant_name')}}</div>
	              @endif
	             </div>
	           </div>
	         </div>
	         
	         <div class="form-group">
	          <div class="row">
	          	<input type="hidden" name="plant_id" id="plant_id">
	            <label class="control-label col-sm-3" for="login_id">Plant Code:<span style="color:#f70b0b;">*</span> </label>
	            <div class="col-sm-9" id="append">
	             <input type="text"  class="form-control plant_code" required="required" name="plant_code" id="plant_Code" placeholder="Enter Plant Code" >
	             @if($errors->has('plant_code'))
	             <div class="error">{{$errors->first('plant_code')}}</div>
	             @endif
	           </div>
	         </div>
	       </div>
	       <div class="form-group">
	          <div class="row">
	            <label class="control-label col-sm-3" for="login_id">Status:<span style="color:#f70b0b;">*</span> </label>
	            <div class="col-sm-9" id="append">
	             <select name="IS_ACTIVE" id="IS_ACTIVE" class="form-control">
	             	<option value="1">Active</option>
	             	<option value="0">De-Active</option>
	             </select>
	           </div>
	         </div>
	       </div>
			 <div class="form-group">
			  <div class="modal-footer">
			    <button type="submit" class="btn btn-primary" id="submitPlant">Update</button>
			  </div>
			</div>
	     </form>
	   </div> 
     </div>
   </div>
</div>

<!-- ============================================ delete Plant-->
<div class="modal fade" id="delete_Plant_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<span class="caption-subject text-danger">Confirmation</span>
			</div> 
			<div class="modal-body" id="info">Are you sure ! You want to delete?</div>
			<div class="modal-footer">
				<form action="{{route('plantMaster.delete')}}" method="post">
					{{csrf_field()}}
					<input type="hidden" name="delete_id" class="delete_id">
					<button type="button" data-dismiss="modal" class="btn btn-success">Cancel</button>
					<button type="submit"  class="btn btn-danger" id="delete"><i class="fa fa-trash"></i> Delete</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection



@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $(".delete").on("submit", function(){
        return confirm("Do you want to update this password?");
    });
});
     $(document).ready(function(){
       $('.plant_code').on('keyup',function(){
      	var plant_code= $('#plant_code').val();
       	  $.ajax({
            type: "get",
            url: "{{route('plantMaster.checkPlantCode')}}",
            data: {plant_code:plant_code},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                  $('#submitPlant').prop('disabled', false);
                  $(".label-important").remove();
                   
                }else{
                   $("#append").append("<span class='label label-important' style='color:red;'>"+"PlantCode Already Registered "+'</span>');
                  $('#submitPlant').prop('disabled', true);
                }

            },
          });
       });

        
             $('.EditPlant').on('click',function(){
             	var plant_id=$(this).data('plant_id');
             	var plant_name=$(this).data('plant_name');
             	var plant_code=$(this).data('plant_code');
             	jQuery('#plant_id').val(plant_id);
             	jQuery('#plant_Name').val(plant_name);
             	jQuery('#plant_Code').val(plant_code);
             	jQuery('#EditPlantModal').modal('show');
             });

                
	            jQuery('.deletePlant').on('click',function () {
		    	var delete_id = jQuery(this).data('plant_id');
		        jQuery('.delete_id').val(delete_id);
		        jQuery('#delete_Plant_modal').modal('show');
	   

	});
	  
});
	jQuery(document).ready(function(){
		$('#table').DataTable();	
	});
    
</script>
@endsection