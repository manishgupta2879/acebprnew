jQuery(function(){
	jQuery('input[type=email],input[type=text],textarea').keyup(function() {
		var v = jQuery(this).val();
		var u = v.toUpperCase();
		if( v != u ) jQuery(this).val(u);
	});
});