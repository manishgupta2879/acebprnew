
<!DOCTYPE html>
<html lang="en">
<head>
	<title>login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet"> -->
	<style type="text/css">

	body {
		background-color: #eee;
		background: url(images/wall1.jpg) no-repeat fixed center center / cover rgba(0, 0, 0, 0);
		/* background: url(images/wall1.jpg) no-repeat center center fixed; */
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	.login-box {
		background-color: #fff;
		border: 1px solid #ce1200;
		border-radius: 4px;
		box-shadow: 0 1px 6px 0 #ce1200;
		height: 250px;
		padding: 20px;
		text-align: left;
		width: 450px;
	}
	.login-header {
		margin-top: 0px;
		border-bottom: 2px #ccc solid;
		padding-left: 25px;
		padding-bottom: 5px;
	}
	.login-header {
		margin-top: 0px;
		border-bottom: 2px #ccc solid;
		padding-left: 25px;
		padding-bottom: 5px;
	}

	.span6 {
		float: right;
		margin-top: 4%;
		/* margin-left: 4%; */
	}
	#loginpage_Footer {
		background:url(images/cranes_80.png) no-repeat scroll 50%;
		width: 100%;
		height: 150px;
		position: relative;
		bottom: -60px;
	}
	.logo {
		margin-top: 1%;
	}
</style>
</head>
<body>
	<div class="container-fluid login-container">
		<div class="row-fluid"><div class="span3">
			<div class="logo">
				<img src="{{URL::to('/')}}/images/login_left_logo.png" alt="ACE" title="ACE">
				<br>
				<a target="_blank" href="http://">
				</a>
			</div>
		</div>
		<div class="span9">
			<div style="float: right; margin-right: 55px; margin-top: -95px;">
				<img src="{{URL::to('/')}}/images/ace80_logo.png" alt="ACE80" title="ace80" width="110px">
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="content-wrapper">
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span6">
							<div class="carousal-container">
							</div>
							<div class="span6">
								<div class="login-area">
									<div class="login-box" id="loginDiv">
										<div class="">
											<h3 class="login-header">Login to <span><img src="{{URL::to('/')}}/images/ace80_logo.png" alt="ACE80" title="ace80" width="30px"></span></h3></div>

											<form  class="form-horizontal login-form" style="margin:0;" action="{{route('login') }}" method="post" autocomplete="off">

												{{csrf_field()}}

												<div class="control-group" style="padding-bottom: 15px;">
													<div class="row">
														<div class="col-md-4">
															<label class="control-label" for="username">
																<b>USER NAME</b>
															</label>
														</div>	
														<div class="col-md-8">	
															<div class="controls">

																<input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  required placeholder="Enter User name" autofocus>

																@if ($errors->has('email'))
																<span class="invalid-feedback">
																	<strong style="text-color:black;">{{ $errors->first('email') }}</strong>
																</span>
																@endif
															</div>
														</div>
													</div>	
												</div>

												<div class="control-group " style="padding-bottom: 15px;">
													<div class="row">
														<div class="col-md-4">
															<label class="control-label" for="password">
																<b>PASSWORD</b>
															</label>
														</div>
														<div class="col-md-8">
															<div class="controls">
																<input id="password" type="password" placeholder="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
																@if ($errors->has('password'))
																<span class="invalid-feedback">
																	<strong>{{ $errors->first('password') }}</strong>
																</span>
																@endif
															</div>
														</div>
													</div>
												</div>
												<div class="control-group signin-button">
													<div class="controls" id="forgotPassword">
														<button style="    margin-left: 36%;" type="submit" class="btn btn-primary sbutton">SIGN IN</button>
														
													</div>
												</div>
											</form>
											<div class="login-subscript"><!--<span><img src="layouts/vlayout/skins/images/tetra_logo.gif" alt="Tetra" title="Tetra"/><small> <b>Supported by Tetra Information Services Pvt. Ltd.</b> </small>--></div></div><div class="login-box hide" id="forgotPasswordDiv"><form class="form-horizontal login-form" style="margin:0;" action="forgotPassword.php" method="POST"><input type="hidden" name="__vtrftk" value="sid:55ea7644074ae874a5c9c0c6327e0fb2338189af,1530195209"><div class=""><h3 class="login-header">Forgot Password</h3></div><div class="control-group"><label class="control-label" for="user_name"><b>User name</b></label><div class="controls"><input type="text" id="user_name" name="user_name" placeholder="Username"></div></div><div class="control-group"><label class="control-label" for="email"><b>Email</b></label><div class="controls"><input type="text" id="emailId" name="emailId" placeholder="Email"></div></div><div class="control-group signin-button"><div class="controls" id="backButton"><input type="submit" class="btn btn-primary sbutton" value="Submit" name="retrievePassword">&nbsp;&nbsp;&nbsp;<a>Back</a></div></div></form></div></div></div></div></div></div></div></div></div><div id="loginpage_Footer"></div><script type="text/javascript">CsrfMagic.end();</script><script>jQuery(document).ready(function(){jQuery("#forgotPassword a").click(function() {jQuery("#loginDiv").hide();jQuery("#forgotPasswordDiv").show();});jQuery("#backButton a").click(function() {jQuery("#loginDiv").show();jQuery("#forgotPasswordDiv").hide();});jQuery("input[name='retrievePassword']").click(function (){var username = jQuery('#user_name').val();var email = jQuery('#emailId').val();var email1 = email.replace(/^\s+/,'').replace(/\s+$/,'');var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;if(username == ''){alert('Please enter valid username');return false;} else if(!emailFilter.test(email1) || email == ''){alert('Please enater valid email address');return false;} else if(email.match(illegalChars)){alert( "The email address contains illegal characters.");return false;} else {return true;}});});</script>
												<input id="activityReminder" class="hide noprint" type="hidden" value="">




											</body>
											</html>
