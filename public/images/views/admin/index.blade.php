@extends('layout.app')
@section('pageTitle')

&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;PASSWORD UPDATE</i>
   
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  

<style type="text/css">
.table >tbody > tr > td
  {
    padding: 1px;
  }
  .buttons-html5{
    background-color: #f37c04!important;
        border-color: #f37c04!important;
    position: relative !important;
    top: 0px !important;
        border-radius: 3px;
            box-shadow: none;
    border: 1px solid transparent;
        display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    float: right;
  }
  #example_length{
  margin-bottom: -33px;
}
.input-sm{
  margin-right: 25px;
}
</style>
<div class="col-md-12" style="text-align:center;">
  <div  style="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div> 
</div>




<div class="col-md-12">
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="padding: 7px 12px;">
   <span class="glyphicon glyphicon-plus"></span>
 </button>
 <br>
 <br>
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">ADD USER</h3>

      </div>
      <div class="modal-body">
        <form autocomplete="off" action="{{ route('admin.create') }}" method="post" >
          {{ csrf_field() }}
          
          
          <div class="form-group">
            <div class="row">
              <label class="control-label col-sm-3" for="name">NAME: <span style="color:#f70b0b;">*</span></label>
              <div class="col-sm-9">
               <input type="text" class="form-control" required="required" name="name" id="name" >
             </div>

           </div>
         </div>
         
         <div class="form-group">
          <div class="row">
            <label class="control-label col-sm-3" for="login_id">USER ID:<span style="color:#f70b0b;">*</span> </label>
            <div class="col-sm-9" id="append">
             <input type="text" onkeyup="duplicateEmail(this)" class="form-control" required="required" name="login_id" id="login_id" >
           </div>
         </div>
       </div>
       <div class="form-group">
        <div class="row">
          <label class="control-label col-sm-3" for="type">USER TYPE:<span style="color:#f70b0b;">*</span> </label>
          <div class="col-sm-9">
           <select class="form-control" required="required" id="type" name="type" >
             <option value="">--Select Type--</option>
             <option value="VENDOR">VENDOR</option>
             <option value="BUYER">BUYER</option>
             <option value="ADMIN">ADMIN</option>
             
           </select>
         </div>
       </div>
     </div>
     <div class="form-group">
       <div class="row">
        <label class="control-label col-sm-3" for="mobile">MOBILE:<span style="color:#f70b0b;">*</span> </label>
        <div class="col-sm-9">
         <input type="text"  class="form-control " required="" id="mobile" name="mobile">
       </div>
     </div>
   </div>
   <div class="form-group">
     <div class="row">
      <label class="control-label col-sm-3" for="password">PASSWORD: <span style="color:#f70b0b;">*</span></label>
      <div class="col-sm-9">
       <input type="password"  class="form-control" required="" id="password" name="password">
     </div>
   </div>
 </div>
 <div class="form-group">
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary" id="submituser">Submit</button>
  </div>
</div>

</form>
</div>

</div>
</div>
</div>

<form class="delete" action="{{ route('admin.update') }}" method="post">
  {{ csrf_field() }} 
  <div class="  table-responsive ">
    <table id="example" class="display  table  table-bordered"  " >
      <thead>
        <tr>     
          <th class="btn-primary">USER NAME</th>
          <th class="btn-primary">USER TYPE</th>
          <th class="btn-primary">LOGIN ID</th>
          <th class="btn-primary">PASSWORD</th>
          <th class="btn-primary">UPDATE</th>
          
          
        </tr>
      </thead>
      <tbody>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
        
        @foreach($users as $data)
        
        <tr>
          <input type="hidden" name="update_id[]" value="{{$data->id}}">
          
          <td>{{$data->name}}</td>
          <td>{{$data->type}}</td>
          <td>{{$data->email}}</td>
          <td><input style="max-width: 85px;" type="password" id="norm{{$data->id}}" name="password[]" value="{{$data->password}}"></td>

          <td>
            <button type="submit" class="btn btn-primary delete" >
             <span class="glyphicon glyphicon-edit"></span>
           </button>
           
         </td>
       </tr>
       @endforeach
       
     </tbody>
   </table>
 </div>
</form>
</div>

</form>


<script>       
        $(document).ready(function() {
                  $('#example').DataTable({
              
                    buttons: [
                        'csv'
                        ],
                       
                        "aaSorting": [],
                    dom: 'Blfrtip',
                    "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]]
                  });


              var table = $('#example').DataTable();
                $("#list_filter input").on('keyup click', function() {
                    table.columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).search($(this).val()).draw();
                });
                 
                $("#number_search").on('keyup click', function() {
                    table.column(10).search($(this).val()).draw();
                });
        } );


    </script>


<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $(".delete").on("submit", function(){
        return confirm("Do you want to update this password?");
    });
});
    function duplicateEmail(element){
        var login_id = $(element).val();
        $.ajax({
            type: "get",
            url: "{{url('checkuserEmail')}}",
            data: {login_ids:login_id},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                  $('#submituser').prop('disabled', false);
                  $(".label-important").remove();
                   
                }else{
                   $("#append").append("<span class='label label-important' style='color:red;'>"+"User Already Registered"+'</span>');
                  $('#submituser').prop('disabled', true);
                }

            },
           
        });
    }
  
</script>

@endsection
