<div class="skin-blue">
<aside class="main-sidebar   >
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
           <i class="fa fa-user fa-lg fa-3x" style="color:white;    margin-top: 8px;"></i>
        </div>
        <div class="pull-left info" style="margin-left: -18px;">
          <p>{{Auth::user()->name}}</p>
          <h6>{{Auth::user()->type}}</h6>
        </div>
      </div>
     
      <ul class="sidebar-menu" data-widget="tree">
        
         @if(Auth::user()->type == "VENDOR")
          <li class=" treeview">
            <a href="{{route('bprdata')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/stock_new_icon.png">
              </i> <span>BPR DATA VIEW</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        
          </li>
          <li class="treeview">
            <a href="{{route('stockdetail')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/grey_stock_icon.png">
              </i>
              <span>STOCK DETAILS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li> 
          @endif
           @if(Auth::user()->type == "BUYER")
          <li class=" treeview">
            <a href="{{route('bprdata')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/stock_new_icon.png">
              </i> <span>BPR DATA VIEW</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>
             <li class="treeview">
            <a href="{{route('stockdetail')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/grey_stock_icon.png">
              </i>
              <span>STOCK DETAILS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li> 
          @endif    
          @if(Auth::user()->type == "ADMIN")    
          @if(Auth::user()->name == "Admin")    
          <li class=" treeview">
            <a href="{{route('admin')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/managepasswords.png">
              </i> <span>MANAGE PASSWORD</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>
            <li class=" treeview">
            <a href="{{route('uom')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/managepasswords.png">
              </i> <span>UOM </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>

           
               <li class=" treeview">
            <a href="{{route('plantMaster.index')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/managepasswords.png">
              </i> <span>Plant </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>

              <li class=" treeview">
            <a href="{{route('uom')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/managepasswords.png">
              </i> <span>Stages </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>
            


           @endif
             <li class=" treeview">
            <a href="{{route('bprdata')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/stock_new_icon.png">
              </i> <span>BPR DATA VIEW</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>
             <li class="treeview">
            <a href="{{route('stockdetail')}}">
              <i class="">
                <img width="15%" src="{{URL::to('/')}}/images/grey_stock_icon.png">
              </i>
              <span>STOCK DETAILS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li> 
          @endif    

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
</div>