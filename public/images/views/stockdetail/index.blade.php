@extends('layout.app')
@section('pageTitle')

&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;STOCK DATA</i>
   
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  

<div class="col-md-12" style="text-align:center;">
@if (Auth::user()->type=="VENDOR")
   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
 <span class="glyphicon glyphicon-plus"></span> 
</button>
@if($stock_list->count()>0)
<a href="{{route('stockdetail.edit')}}">
<button type="button" class="btn btn-primary" >
 <span class="glyphicon glyphicon-edit"></span>
</button>
</a>
@endif
@endif
</div>
 <div class="col-md-12"><br>
<style type="text/css">
td{font-size: 12px;}
  .buttons-html5{
    background-color: #f37c04!important;
        border-color: #f37c04!important;
    position: relative !important;
    top: 0px !important;
        border-radius: 3px;
            box-shadow: none;
    border: 1px solid transparent;
        display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    float: right;
  }
  #example_length{
  margin-bottom: -33px;
}
.input-sm{
  margin-right: 25px;
}
</style>
<table>
                                <thead>
                                <tr> 
                                    
                          <span style="margin-left: 35%" >
               

                            <a href="#" class="button color" data-color="Black" style="background:#000;color:#fff;border-radius: 10px;width: 70px;">{{$Black}}</a>
                            <a href="#" class="button color" data-color="Red"  style="background:#FF0000;color:#fff;border-radius: 10px;width: 70px;">{{$Red}}</a>
                            <a href="#" class="button color" data-color="Yellow" style="background:#FFFF00;border-radius: 10px;width: 70px;" >{{$Yellow}}</a>
                            <a href="#" class="button color" data-color="Green" style="background:#64FF33;border-radius: 10px;width: 70px;">{{$Green}}</a>
                            <a href="#" class="button color" data-color="White" style="background:#fff;color:#000;border-radius: 10px;width: 70px;">{{$White}}</a>
                                       
                                       
                         
                                  </span>
                                  @if(Auth::user()->type=="VENDOR")
                                  
                                     @endif
                                </tr>
                            </thead>
                           
                                    </table>

<div class="  table-responsive ">
                    <table  id="example" class="display  table  table-bordered" >
                    <thead>
                        <tr>     
                            <th class="btn-primary">LAST UPDATED</th>
                            <th class="btn-primary">ITEM</th>
                            <th class="btn-primary">TYPE</th>
                            <th class="btn-primary">UOM</th>
                            <th class="btn-primary">NORM</th>
                            <th class="btn-primary">FG/STORE STOCK</th>
                            <th class="btn-primary">WIP STOCK</th>
                            <th style="min-width: 100px;" class="btn-primary">TO PROCURE/ PRODUCE</th>
                            <th class="btn-primary">REMARK</th>
                          @if(Auth::user()->type=="ADMIN" || Auth::user()->type=="BUYER" )
                          <th class="btn-primary">VENDOR NAME</th>

                            @endif
                          <th class="btn-primary">ECO COLOR</th>
                          @if(Auth::user()->type=="VENDOR")
                          <th class="btn-primary"></th>
                          <th class="btn-primary"></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($stock_list as $data)
                        <tr>
                            <td>{{Carbon\Carbon::parse($data->updated_date)->format('d/m/Y')}}</td>
                            <td>{{$data->item}}</td>
                            <td>{{$data->type}}</td>
                            <td>{{$data->UOM}}</td>
                            <td>{{$data->norm}}</td>
                            <td>{{$data->fg_store}}</td>
                            <td>{{$data->wip_stock}}</td>
                            <td>{{$data->procure}}</td>
                            <td>{{$data->remark}}</td>
                            @if(Auth::user()->type=="ADMIN"|| Auth::user()->type=="BUYER")
                            <td>{{$data->VENDOR_NAME}}</td>
                            @endif
                            <td @switch($data->color)
                                        @case ("Black")
                                            style="background-color:#000;color:#fff;"
                                            @break
                                        @case ("Red")
                                            style="background-color:#FF0000;"
                                            @break
                                        @case ("Yellow")
                                            style="background-color:#FFFF00;"
                                            @break    
                                        @case ("Green")
                                            style="background-color:#64FF33;"
                                            @break
                                        @default
                                            style="background-color:#fff;color:#000;"
                                            @break
                                        @endswitch>{{$data->color}}</td>
                              @if(Auth::user()->type=="VENDOR")
                              <td>
                                 <div class="btn-group">
                                  <form class="delete" action="{{ route('stockdetail.destroy') }}" method="post">
                                  <input type="hidden" name="id" value="{{$data->id}}">
                                  {{ csrf_field() }}
                                  <button type="submit" style="background-color: #fff;border: none;" class="">
                                    <i class="fa fa-remove" style="font-size:30px;color:red"></i>
                                    <!-- <img style="width: 10%;" src="{{URL::to('/')}}/images/remove.png"> -->
                                  </button>
                                 </form>
                                </div>
                              </td>
                              <td>
                                 <div class="btn-group">
                                  <form class="" action="{{ route('stockdetail.oneedit') }}" method="get">
                                  <input type="hidden" name="id" value="{{$data->id}}">
                                  
                                  <button type="submit" style="background-color: #fff;" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    <!-- <img style="width: 10%;" src="{{URL::to('/')}}/images/remove.png"> -->
                                  </button>
                                 </form>
                                </div>
                              </td>
                              @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

</div>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">Add Stock</h3>



        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" action="{{ route('stockdetail.create') }}" method="post" autocomplete="off">
        {{ csrf_field() }}
                          
                    <input type="hidden" name="login_id" value="{{Auth::user()->login_id}}">
                          <div class="form-group">
                            <div class="row">
                          <label class="control-label col-sm-3" for="email">Item: <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                               <input type="text" onblur="duplicateEmail(this)" class="form-control" required="required" name="item" id="" >
                          </div>

                          <span class="text-danger" style="margin-left: 28%;"></span>
                          </div>
                        </div>
                       
                          <div class="form-group">
                                                         <div class="row">
                          <label class="control-label col-sm-3" for="email">Type: </label>
                          <div class="col-sm-9">
                               <select class="form-control" required="required" id="type" name="type">
                                    <option value="">--select type--</option>
                                    @foreach($type as $types)
                                    <option  value="{{$types->name}}">{{$types->name}}</option>
                                    @endforeach
                                </select>

                          </div>
                          </div>
                        </div>
                         <div class="form-group">
                            <div class="row">
                          <label class="control-label col-sm-3" for="email">UOM: </label>
                          <div class="col-sm-9">
                             <select class="form-control checkunique" required="required" id="texat" name="UOM" >
                                   <option value="">--select UOM--</option>
                                      @foreach($uom_type as $types)
                                        <option  value="{{$types->name}}">{{$types->name}}</option>
                                    @endforeach
                              </select>
                          </div>
                          </div>
                        </div>
                        <div class="form-group">
                             <div class="row">
                          <label class="control-label col-sm-3" for="email">Norm: </label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control numeric procure" required="" id="procure_norm" name="norm">
                          </div>
                          </div>
                        </div>
                       
                         <div class="form-group">
                            <div class="row">
                          <label class="control-label col-sm-3" for="email">FG/Store Stock: </label>
                          <div class="col-sm-9">
                               <input type="text"  class="form-control numeric procure" id="procure_fg_stoc" required=""  name="fg_store"  >
                          </div>
                          </div>
                        </div>

                         <div class="form-group WIP">
                                                         <div class="row">
                          <label class="control-label col-sm-3" for="pwd">WIP Stock:</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control numeric procure" name="wip_stock" id="procure_wip"  > 
                          </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="row">
                          <label class="control-label col-sm-3" for="pwd">To Procure/ Produce: </label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control numeric" name="procure" required=""  readonly="readonly" id="procure">
                          </div>
                          </div>
                        </div>
                        <div class="form-group">
                                                         <div class="row">
                          <label class="control-label col-sm-3" for="pwd">Remark: </label>
                          <div class="col-sm-9">
                            <textarea name="remark" style="width:100%;" ></textarea>
                          </div>
                          </div>
                        </div>
                        
                    
                          
                        <div class="form-group">
                          <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="save_changes">Save changes</button>
      </div>
                        </div>
     
        </form>
      </div>
      
    </div>
  </div>
</div>
 
                    



@endsection

    @section('script')

   <script>       
        $(document).ready(function() {
            $('#example').DataTable({
              
              buttons: [
                  'csv'
                  ],
                 
                  "aaSorting": [],
              dom: 'Blfrtip',
              "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]]
            });

              var table = $('#example').DataTable();
                $("#list_filter input").on('keyup click', function() {
                    table.columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).search($(this).val()).draw();
                });
                 
                $("#number_search").on('keyup click', function() {
                    table.column(10).search($(this).val()).draw();
                });
        } );
    </script>


<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>


    <script type="text/javascript">


        jQuery(function(){
            var vil={
                init:function(){

                    vil.date();
                    console.log('hello');


               $('.color').click(function() {debugger;
                      var color = $(this).data('color');   
                      jQuery("input[type='search']").val(color);
                      var table = $('#example').DataTable();
                        <?php if (Auth::user()->type=="VENDOR" ) {
                            ?>
                            table.column(9).search(color).draw();
                        <?php  }else{ ?>

                            table.column(10).search(color).draw();
                      <?php  } ?>
                });
               
                },

                date:function(){
                    jQuery(".datepicker").datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        format: 'dd/mm/yyyy'
                    });

                },


                
            }
            vil.init();
        });

                jQuery(document).ready(function()
                    {      

                    $("select").on('change',function(){     

                     var type = $("#type").val();

                     if (type == 'FG') {

                      $('.WIP').hide(1000);  

                      
                     }
                     if (type == 'RM') {

                      $('.WIP').show(1000);  
                     
                     }

                    });           
                    
                    });

                $(document).on('keyup', '.numeric', function(event) {
                        var v = this.value;
                         if($.isNumeric(v) === false) {
        //chop off the last char entered
                        this.value = this.value.slice(0,-1);
                     }
                });
        //Calculate Procure      
                 $(document).on('keyup', '.procure', function() {

                  var procure_norm =0;
                  var procure_fg_stoc =0;
                  var procure_wip =0;  
                         procure_norm = ($('#procure_norm').val()?$('#procure_norm').val():0);
                         procure_fg_stoc = ($('#procure_fg_stoc').val()?$('#procure_fg_stoc').val():0);
                     
                         procure_wip = ($('#procure_wip').val()?$('#procure_wip').val():0);

                        var procure_cal = (parseFloat(procure_norm)-(parseFloat(procure_fg_stoc)+parseFloat(procure_wip)));

                        if(isNaN(procure_cal)){
                              procure_cal=0;
                          }
                          if (procure_cal <= 0) {
                              procure_cal=0;
                          }
                      
                      $('#procure').val(procure_cal.toFixed(2));
                     
                });

                       $('#texat').on('select',function (e) {
                            var regex = new RegExp("^[a-zA-Z0-9]+$");
                            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                            if (regex.test(str)) {
                                return true;
                            }

                            e.preventDefault();
                            return false;
                        });
            
jQuery(function(){
  jQuery('input[type=email],input[type=text],textarea').keyup(function() {
    var v = jQuery(this).val();
    var u = v.toUpperCase();
    if( v != u ) jQuery(this).val(u);
  });
});


    </script>
    <script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });

$(".text-danger").hide();
 function duplicateEmail(element){
        var item = $(element).val();
        $.ajax({
            type: "get",
            url: '{{url('checkemail')}}',
            data: {items:item},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                  $(".text-danger").text("");
                  $(".text-danger").hide();
                  $('#save_changes').prop('disabled', false);
                }else{

                  
                  $(".text-danger").text("Item already exists.");
                  $(".text-danger").show();
                  $('#save_changes').prop('disabled', true);
                    // alert('false');

                }

            },
            error: function (jqXHR, exception) {

            }
        });
    }

</script>


    @endsection