@extends('layout.app')
@section('pageTitle')
<i class="fa fa-dashboard">BPR Data</i>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
      <!-- <i class="fa fa-trash-o" aria-hidden="true" title="Delete"></i> <span class="border"></span> <i class="fa fa-save" aria-hidden="true" title="Save"></i>  -->
        <div class="content-section">
          
        
            <div class="  table-responsive ">
                <table class="table table-bordered search-table " >
                    <thead>
                        <tr>     
                            <th>BPR_DATE</th>
                            <th>INVENTORY_LOCATION</th>
                            <th>ITEM_CODE</th>
                            <th>DESCRIPTION</th>
                            <th>UOM</th>
                            <th>ECO_PERCENT</th>
                            <th>ECO_PENE_COLOUR</th>
                            <th>QUANTITY</th>
                        </tr>
                    </thead>
                    <tbody>
                     @foreach($bpr_list as $data)
                        <tr>
                            
                            <td>{{$data->BPR_DATE}}</td>
                            <td>{{$data->INVENTORY_LOCATION}}</td>
                            <td>{{$data->ITEM_CODE}}</td>
                            <td>{{$data->DESCRIPTION}}</td>
                            <td @switch($data->UOM)
                            		@case ("Red")
                            			style="background-color:#FF0000;"
                            			@break
                            		@case ("Green")
                            			style="background-color:#64FF33;"
                            			@break
                            		@case ("Yellow")
                            			style="background-color:##FFFF00;"
                            			@break
                            		@default
                            			style="background-color:##FFFF00;"
                            			@break
                            	@endswitch>{{$data->UOM}}</td>
                            <td>{{$data->ECO_PERCENT}}</td>
                            <td>{{$data->ECO_PENE_COLOUR}}</td>
                            <td>{{$data->QUANTITY}}</td>
                       
                        </tr>
                   @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection