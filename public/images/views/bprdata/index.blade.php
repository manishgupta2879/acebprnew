@extends('layout.app')
@section('pageTitle')
&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;BPR DATA</i>
@endsection
@section('content')

@if(empty($bpr_list))
<div class="col-md-12">
    <div class="content-section">
         <?php $mytime = Carbon\Carbon::now()->format('d/m/Y'); ?>
            <div class="row">
                <form  autocomplete="off" id="form1" class="form-horizontal" action="{{route('bprdata')}}" method="get">
                    
                <div class="col-md-3"> 
                </div>
               
                <div class="col-md-6">
                    
                    <div class="input-group">
                        <input type="text" class="form-control datepicker"  name="from_date" id="datepicker" placeholder="Enter BPR Date.."  >
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" id="btnSearchInv"><span class="glyphicon glyphicon-search"></span> SEARCH</button>
                        </span>
                    </div>
                    
                </div>
                 <div class="col-md-3"> 
                </div>
                </form>
            </div>
            <!-- <br>
                            <table>
                                <thead>
                                <tr> 
                                    <span style="margin-left: 35%" >
                                        <?php
                                                $user_info2 = array('Black','Red','Yellow','Green'); 
                                        ?>
                @foreach($user_info2 as $users_infos)
                          
                            <a href="#" class="button" @switch($users_infos)
                                        @case ("Red")
                                            style="background:#FF0000;color:#fff;border-radius: 10px;width: 70px;"
                                            @break
                                        @case ("Green")
                                            style="background:#64FF33;border-radius: 10px;width: 70px;"
                                            @break
                                        @case ("Yellow")
                                            style="background:#FFFF00;border-radius: 10px;width: 70px;"
                                            @break
                                        @default
                                            style="background:#000;color:#fff;border-radius: 10px;width: 70px;"
                                            @break
                                    @endswitch >{{$users_infos}}</a>
                        @endforeach
                        </span>
                       
                          
                                </tr>
                            </thead>
                                    </table> -->
        </div>
    </div>
 
   @endif
    




<style type="text/css">
td{font-size: 12px;}
  .buttons-html5{
    background-color: #f37c04!important;
        border-color: #f37c04!important;
    position: relative !important;
    top: 0px !important;
        border-radius: 3px;
            box-shadow: none;
    border: 1px solid transparent;
        display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    float: right;
  }
  #example_length{
  margin-bottom: -33px;
}
.input-sm{
  margin-right: 25px;
}
</style>
@if(!empty($bpr_list))
        <div class="col-md-12">
            <div class="content-section">
                <div class="row">
                    <form autocomplete="off" class="form-horizontal" action="{{route('bprdata')}}" method="get">  
                        
                    
                <div class="col-md-3"> 
                   
                </div>
               
                <div class="col-md-6">
                    
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" name="from_date" id="datepickers" placeholder="Enter BPR Date.." value="@if(isset($from_date)){{Carbon\Carbon::parse($from_date)->format('d/m/Y')}}@endif" autocomplete="off">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" id="btnSearchInv"><span class="glyphicon glyphicon-search"></span> SEARCH</button>
                        </span>
                    </div>
                    
                </div>
                 <div class="col-md-3"> 
                    <div class="input-group">
                            
                    </div>
                </div>
              
                    </form>
                </div><br>
                            <table>
                                <thead>
                                <tr> 
                                        <?php
                                                $url="?from_date=".$_GET['from_date'];
                                        ?>
                    <span style="margin-left: 35%" >
                                      
                            <a href="#" class="button color" data-color="Black" style="background:#000;color:#fff;border-radius: 10px;width: 70px;">{{$Black}}</a>
                            <a href="#" class="button color" data-color="Red"  style="background:#FF0000;color:#fff;border-radius: 10px;width: 70px;">{{$Red}}</a>
                            <a href="#" class="button color" data-color="Yellow" style="background:#FFFF00;border-radius: 10px;width: 70px;" >{{$Yellow}}</a>
                            <a href="#" class="button color" data-color="Green" style="background:#64FF33;border-radius: 10px;width: 70px;">{{$Green}}</a>
                            <a href="#" class="button color" data-color="White" style="background:#fff;color:#000;border-radius: 10px;width: 70px;">{{$White}}</a>
                        </span>
                       
                          
                                </tr>
                            </thead>
                                    </table>

                                    
                <div class="  table-responsive ">
                    <table  id="example" class="display  table  table-bordered" >
                        <thead>
                            <tr>    
                                <th class="btn-primary">BPR DATE</th>
                                <th style="width: 150px;" class="btn-primary">INVENTORY LOCATION</th>
                                <th  class="btn-primary">ITEM CODE</th>
                                <th  class="btn-primary">DESCRIPTION</th>
                                <th  class="btn-primary">UOM</th>
                                <th  class="btn-primary">ECO %</th>
                                @if(Auth::user()->type=="VENDOR")
                                <th  class="btn-primary">BUYER NAME</th>
                                @elseif(Auth::user()->type=="BUYER")
                                <th  class="btn-primary">VENDOR NAME</th>
                                @elseif(Auth::user()->type=="ADMIN")
                                <th  class="btn-primary">BUYER NAME</th>
                                <th  class="btn-primary">VENDOR NAME</th>
                                @endif
                                <th  class="btn-primary">ECO COLOUR</th>
                                <th  class="btn-primary">QTY.</th>
                            </tr>
                        </thead>
                        <tbody>
                         @foreach($bpr_list as $data)
                            <tr>
                                <td>{{Carbon\Carbon::parse($data->BPR_DATE)->format('d/m/Y')}}</td>
                                <td>{{$data->INVENTORY_LOCATION}}</td>
                                <td>{{$data->ITEM_CODE}}</td>
                                <td>{{$data->DESCRIPTION}}</td>
                                <td >{{$data->UOM}}</td>
                                <td>{{$data->ECO_PERCENT}}%</td>
                                @if(Auth::user()->type=="VENDOR")
                                <td>{{$data->BUYER_NAME}}</td>
                                 @elseif(Auth::user()->type=="BUYER")
                                 <td>{{$data->VENDOR_NAME}}</td>
                                 @elseif(Auth::user()->type=="ADMIN")
                                <td>{{$data->BUYER_NAME}}</td>
                                 <td>{{$data->VENDOR_NAME}}</td>
                                @endif
                                <td class="ECO_PENE_COLOUR" @switch($data->ECO_PENE_COLOUR)
                                        @case ("Black")
                                            style="background-color:#000;color:#fff"
                                            @break
                                        @case ("Red")
                                            style="background-color:#FF0000;"
                                            @break
                                        @case ("Green")
                                            style="background-color:#64FF33;"
                                            @break
                                        @case ("Yellow")
                                            style="background-color:#FFFF00;"
                                            @break
                                        @default
                                            style="background-color:#fff;color:#000"
                                            @break
                                    @endswitch>{{$data->ECO_PENE_COLOUR}}</td>
                                <td>{{$data->QUANTITY}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>      
        @endif
    </div>
</div>


@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
<script type="text/javascript">


    jQuery(function(){
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = 
            (month<10 ? '0' : '') + month + '/' +
            (day<10 ? '0' : '') + day + '/' + d.getFullYear();

        // alert(output);
        var vil={
            init:function(){

                vil.date();
                vil.newdate();

               $('.color').click(function() {
                      var color = $(this).data('color');   
                      jQuery("input[type='search']").val(color);
                      var table = $('#example').DataTable();
                        <?php if (Auth::user()->type=="VENDOR" || Auth::user()->type=="BUYER") {
                            ?>
                            table.column(7).search(color).draw();
                        <?php  }else{ ?>

                            table.column(8).search(color).draw();
                      <?php  } ?>
                });
            },


            date:function(){
                jQuery("#datepicker").datepicker({todayHighlight:true,format: 'dd/mm/yyyy'}).datepicker('setDate',new Date());

            },
            newdate:function(){
                jQuery("#datepickers").datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    key: "",
                    local: false,
                    autoStore: false,
                    setDate: null,
                    clearText:'Clear',
                    showButtonPanel: true,
                    defaultDate: null,
                    format: 'dd/mm/yyyy'
                });

            },
            
        }
        vil.init();
    });

</script>
       <script>       
        $(document).ready(function() {
            $('#example').DataTable({
              
              buttons: [
                  'csv'
                  ],
                 
                  "aaSorting": [],
              dom: 'Blfrtip',
              "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]]
            });

              var table = $('#example').DataTable();
                $("#list_filter input").on('keyup click', function() {
                    table.columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).search($(this).val()).draw();
                });
                 
                $("#number_search").on('keyup click', function() {
                    table.column(10).search($(this).val()).draw();
                });
        } );
    </script>


<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>


@endsection
