
/////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
Controller
///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

<?php

public function todayAttendance()
    {
    	Log::info('AsAttendanceController@todayAttendance');
    	$heading_text = 'Today Attendance Report for all employees';

    	$columns = ['Status','Employee Name','Assigned State', 'Mobile','Selected Route','Live Location', 'Leave Reason', 'Start Time','End Time'];

    	$columns_js = [
            'station_status' => 'station_status',
			'u_name' => 'uses_tracking.u_name',
            
            'u_state' => 'u_state',
			'mobile_text' => 'mobile_text',
			'route_text' => 'route_text',
			'selected_town'=>'selected_town',
			'leave_reason' => 'leave_reasons',
			'start_time' => 'start_time',
			'end_date' => 'end_date'
//			'hours' => 'hours',
    	];
        
    	$state_employees = UsersTrack::pluck('assigned_state as state_id', 'uid_priv as id');
    	$ajax_url = '/track/attendance';
    	$states = DB::table('master_states') ->pluck('state_name', 'id');
        
    		
    	$ajax_param = "";
        $tenantIdArray= Landlord::getTenants('company_id');
           $tenantId=$tenantIdArray['company_id']; 
        $emplyees_query = DB::table('users_tracking')
                    ->join('employee_roles', 'employee_roles.emp_id', '=', 'users_tracking.uid_priv')
                    ->join('roles', 'roles.id', '=', 'employee_roles.role_id')
                    ->where('users_tracking.u_name', '!=', "")

                    ->where('users_tracking.company_id',$tenantId)
                    ->select( DB::raw('CONCAT(users_tracking.u_name,"(",roles.name,")" ) AS text'),'uid_priv as id', 'assigned_state as state_id')
                    ->orderBy('users_tracking.u_name');
        $emplyees = $emplyees_query->get();
        

       
        return view('display.reports_attendance_today')
	    	->with('heading_text', $heading_text)
	    	->with('columns', $columns)
	    	->with('columns_js', $columns_js)
	    	->with('ajax_url', $ajax_url)
	    	->with('ajax_param', $ajax_param)
	    	->with('emplyees', $emplyees)
            ->with('states', $states)
             
                
    	;
    }
    public function todayAttendanceLoadData(Request $request)
    {
        $input=$request->all();
        Log::info('AsAttendanceController@todayAttendanceLoadData(): $request'.print_r($input, true));
        $matchingText = (isset($input['matchingText']))?$input['matchingText']: "";
        Log::info('AsAttendanceController@todayAttendanceLoadData(): matchingText'.print_r($matchingText, true));
        //$selectedState =  (isset($input['state_id']))?$input['state_id']: 0;
        $selectedStates = []; 
            $array_input = Input::get('state_id');
            Log::info('AsAttendanceController@todayAttendanceLoadData	 $array_input - '.print_r($array_input,true));
                if ($array_input && count($array_input)> 0) {
                            $selectedStates= $array_input;
                }

        $tenantIdArray= Landlord::getTenants('company_id');
           $tenantId=$tenantIdArray['company_id']; 
       
        $today = date("Y-m-d");
        Log::info('AsAttendanceController@todayAttendanceLoadData, comId - '.$tenantId.': $today: '.$today);
        $query = DB::table('users_tracking')
                    ->leftJoin('daily_status_summaries', 	function($join) use ($tenantId , $today)
                            {
                              $join->on('daily_status_summaries.user_id', '=', 'users_tracking.uid_priv')
                                    ->where('daily_status_summaries.company_id',  '=', $tenantId)
                                    ->where('daily_status_summaries.date', '=', $today);
                            })
                    ->join('master_states','users_tracking.assigned_state','=','master_states.id')
                    ->where('users_tracking.company_id',  '=', $tenantId)					
                    ->where('users_tracking.u_name',  '!=', '')	
                    ->where('users_tracking.ud_fcmid', '!=','')
                  
                    ->where('users_tracking.assigned_state','!=',0)
                    ->groupBy('users_tracking.uid_priv')
                    //->where('daily_status_summaries.date', '=', $today)
                    ->selectRaw(
                                'users_tracking.uid_priv as uid,master_states.state_name as u_state,
                                users_tracking.u_name,users_tracking.u_mobile,
                                DATE_FORMAT(daily_status_summaries.time_route_selection, \'%h:%i%p\') as start_time,
                                DATE_FORMAT(daily_status_summaries.time_report_submit, \',%h:%i%p\') as end_time,
                               
                                daily_status_summaries.*, TIMESTAMPDIFF(HOUR,daily_status_summaries.time_route_selection, daily_status_summaries.time_report_submit) as hours,daily_status_summaries.selected_town'
                                ) ;  

         if(count($selectedStates) > 0) {
            $query  = $query->whereIn('users_tracking.assigned_state', $selectedStates);			             
        }
        $data=$query->get(); 

  
   Log::info('attendance data $collectionNew'.print_r($collectionNew,true));

   
  
   	return Datatables::of($collectionNew)->make(true);
    }

    ?>
///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
Blade file
///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

<style>
.tooltip {
    font-size: 15px !important;
}
</style>
<div class="container-fluid">
 <fieldset class="ybc-fieldset">
                <legend class="ybc-legend">{!! $heading_text !!}</legend>
    <div class="row">
		<div class="col-md-12" style="margin-bottom: 8px;">

       
	{!! Form::open(array('name' => 'myHtmlForm', 'id' => 'id-html-form', 'class' => 'form-inline', 'method' => 'post')) !!}

@role('Admin')                    
        {!! Form::select('selectedState',$states, NULL, ['multiple' => 'multiple', 'id' => 'id-select-state' ,'class' => 'form-control col-md-2 col-md-offset-2']) !!}
@else
        {!! Form::select('selectedState',$states,explode(",",Auth::user()->route_head_station), ['multiple' => 'multiple', 'id' => 'id-select-state' ,'class' => 'form-control col-md-2 col-md-offset-2', 'disabled' => 'disabled']) !!}
@endrole                    

        {!! Form::button('view', [ 'class' => 'btn btn-primary  form-control', 'id' => 'html-id-filterSubmit']) !!}
    	{!! Form::input('text', 'matching-text', "All", ['id' => 'id-hidden-matching-text', 'hidden' => 'true']) !!}
    					
    {!! Form::close() !!}
		</div>

        <div class="flash-message" style="margin: 20px;">
				  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				    @if(Session::has('alert-' . $msg))
				    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				    @endif
				  @endforeach
        </div>
                             <div class="flash-message" id="id-flash-message" style="margin: 20px;"> </div> 
                             <div class="row" >
                                    <div class="col-md-4">
                                        <div class="col-md-8">
          <!-- small box -->
                                        <div class="small-box bg-aqua">
                                            <div class="inner">
                                                    <h3 id = "id-leave-counts">{{$leaveCounts}}/{{$totalCounts}}</h3>

                                                    <p> Leave Counts</p>
                                            </div>
                                            <div class="icon">
                                                    <i class="ion ion-bag"></i>
                                            </div>
                                            <a href="#" id="id-leave-button" class="small-box-footer">Click to View Details <i class="fa fa-arrow-circle-right"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <!-- small box -->
                                        <div class="col-md-8">
                                        <div class="small-box bg-green">
                                          <div class="inner">
                                            <h3 id="id-online-counts">{{$onlineCounts}}/{{$totalCounts}}</h3>

                                            <p>Online Counts</p>
                                          </div>
                                          <div class="icon">
                                            <i class="ion ion-stats-bars"></i>
                                          </div>
                                          <a href="#" id="id-online-button"class="small-box-footer">Click to View Details <i class="fa fa-arrow-circle-right"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <!-- small box -->
                                            <div class="col-md-8">
                                            <div class="small-box bg-yellow">
                                              <div class="inner">
                                                 
                                                <h3 id="id-offline-counts">{{$offlineCounts}}/{{$totalCounts}} </h3>

                                                <p>Offline Counts&nbsp;&nbsp;&nbsp;&nbsp; <u> <a id="id-alert-button">Send Alert to all</a></u></p>
                                                
                                                  
                                              </div>
                                              
                                              <a href="#" id="id-offline-button" class="small-box-footer">Click to View Details <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            </div>
                                    </div>
                                 </div>
               <hr class="ybc-seprator">
				<div class="panel-body" id="id-html-display-panel">
					<div class = "table-responsive">
                                            <table class="table table-bordered" id="id-html-data-table">
                                                <thead>
                                                    <tr>
                                                        @foreach($columns as $column)
                                                            <th> {{ $column }} </th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                            </table>
	   				</div>
   				</div>
			<!-- </div> -->
            </fieldset>
		</div>
    <!-- model to show login detail -->
<div class="modal fade" id="viewTownDetails">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Town Attendance History</h4>
            </div>
            <div class="modal-body">
                <div id="modal-content-data">
                            <div class="box-body" >
                                <!-- <input type="hidden" name="orderId" id="id-orderId"> -->
                                        <div class="table-responsive" style="height:300px">
                                          <table class="table table-bordered" id="id-table-town_attendance_history" >
                                                <thead>
                                                <tr>
                                                   
                                                    <th>Selected Town</th>
                                                    <th>Location</th>
                                                    <th>Purpose</th>
                                                    <th>Time</th>
                                                    <th>Change Reason</th>
                                                   
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                          </table>  
                                        </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="col-md-5 col-md-offset-2"></div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>







<script>
$(function() {

    var ajax_url = "{{ $ajax_url }}";
    var current_vid =0; 
    var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column ) {
                    // Strip $ from salary column to make it numeric
                    if(column === 2){
                       data= data.substring(0, 10);

                    } 
                    return data;
                        
                }
            },
            columns: ':visible'
        },
        customize: function (xlsx) {
                        console.log(xlsx);

                                $('row:nth-child(3) c:nth-child(4)', sheet).attr('s','2');
                                $('row:nth-child(4) c:nth-child(4)', sheet).attr('s','3');
                                $('row:nth-child(5) c:nth-child(4)', sheet).attr('s','4');
                                $('row:nth-child(6) c:nth-child(4)', sheet).attr('s','5');
                                $('row:nth-child(7) c:nth-child(4)', sheet).attr('s','6');
                                $('row:nth-child(8) c:nth-child(4)', sheet).attr('s','7');
                                $('row:nth-child(9) c:nth-child(4)', sheet).attr('s','8');
                                    $('row:nth-child(18) c:nth-child(4)', sheet).attr('s','17');




                        var sheet = xlsx.xl.worksheets['sheet1.xml'];

                        var downrows = 5;
                        var clRow = $('row', sheet);
                        //update Row
                        clRow.each(function () {
                                var attr = $(this).attr('r');
                                var ind = parseInt(attr);
                                ind = ind + downrows;
                                 $(this).attr("r",ind);
                        });
 
        // Update  row > c
                        $('row c ', sheet).each(function () {
                                var attr = $(this).attr('r');
                                var pre = attr.substring(0, 1);
                                var ind = parseInt(attr.substring(1, attr.length));
                                ind = ind + downrows;
                                $(this).attr("r", pre + ind);
                        });
 
                        function Addrow(index,data) {
                                    msg='<row r="'+index+'">'
                                    for(i=0;i<data.length;i++){
                                                var key=data[i].k;
                                                var value=data[i].v;
                                                msg += '<c t="inlineStr" r="' + key + index + '">';
                                                msg += '<is>';
                                                msg +=  '<t>'+value+'</t>';
                                                msg+=  '</is>';
                                                msg+='</c>';
                                    }
                                    msg += '</row>';
                                    return msg;
                        }
                        var selectedState = $('#id-select-state :selected').text();
                        if (selectedState === '') {
                            selectedState = 'All';
                        }        
                        var timePeriod = new Date().toDateString();
                        
                        var r1 = Addrow(1, [{ k: 'A', v: '' }, { k: 'B', v:'Attendance Report' },{k:'C',v:''}]);
                        var r2 = Addrow(2, [{ k: 'A', v: 'State' }, { k: 'B', v: selectedState}]);
                        var r3 = Addrow(3, [{ k: 'A', v: 'Time Period' }, { k: 'B', v: timePeriod  }]);
                        var r4 = Addrow(4, [{ k: 'A', v: '' }, { k: 'B', v: ''  }]);
        
                        sheet.childNodes[0].childNodes[1].innerHTML = r1+r2+r3+r4+ sheet.childNodes[0].childNodes[1].innerHTML;
                }
    };
    
    $('#id-html-data-table').dataTable({

        
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    if ( aData['route_text'] == "Offline" )
                    {
                        $('td', nRow).css('background-color', '#FADBD8');
                    }
                    else if ( aData['route_text'] == "On Leave" )
                    {
                        $('td', nRow).css('background-color', '#FCF3CF');
                    }
                    else 
                    {
                        $('td', nRow).css('background-color', '#C9FCA1');
                    }


                    if ( aData['station_status'] == 1 )
                    {
                        $(nRow).find('td:eq(2)').css('background-color', '#8AE641');
                    }else if ( aData['station_status'] == 2 )
                    {
                        $(nRow).find('td:eq(2)').css('background-color', '#FFA07A');
                    }
                    

                },
         processing: true,
        serverSide: false,
	lengthMenu: [[100, 200, -1], [100, 200, "All"]],
	dom: 'lBfrtip',
        buttons:[$.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5'
            } ),'colvis'],
        
        language: {
            buttons: {
                colvis: 'Select columns'
            }
        },
        ajax: {
        	"url": ajax_url + '/data',
        	"data": function ( d ) {
                return $.extend( {}, d, {
                        "state_id":$('#id-select-state').val(),
                        "matchingText": $('#id-hidden-matching-text').val(),
                  } );
               }
    	},
        columns: [
			@foreach($columns_js as $key => $value)
			@if ($key === "u_name" )
		            { data: "{{ $key }}", name: "{{ $value }}", searchable: true, orderable: false},
	            	@else
		            { data: "{{ $key }}", name: "{{ $value }}", searchable: false, orderable: false },
		        @endif    
			@endforeach
        
        		],
                columnDefs: [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
                
     
    });
     

    $("#id-leave-button").click(function(){
                $('#id-hidden-matching-text').val('On Leave');
                $('#id-html-data-table').DataTable().ajax.reload();
    } );
    
    
    $("#id-online-button").click(function(){
            
            $('#id-hidden-matching-text').val('Present');
            $('#id-html-data-table').DataTable().ajax.reload();
    } );
    
    $("#id-offline-button").click(function(){
           
            $('#id-hidden-matching-text').val('');
            $('#id-html-data-table').DataTable().ajax.reload();
    } );
    
    $("#id-alert-button").click(function(){
            $('#id-hidden-matching-text').val('');
           
            $.ajax({
                    "url": ajax_url + "/send/alert",
                    "type": "POST",
                    "headers": {
                                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                    },
                    "data": { "matchingText": $('#id-hidden-matching-text').val()},
                    success: function(data){
                    alert("Attendance reminder has been sent to ALL.")
                            //$('#id-html-data-table').DataTable().ajax.reload();
                    },
                    error(error){
                    alert("Error: " + JSON.stringify(error));
                    }
            });
    } );
    
    $("#html-id-filterSubmit").click(function() {
    
    
	
        $("#id-flash-message").html("");
        var state_id=$('#id-select-state').val();
                $('#id-html-data-table').DataTable().ajax.reload();
    });
 



        $('#id-html-data-table').on( 'click', 'button', function () {
	
		var data_toggle = $(this).attr('data-toggle');
		var current_vid = $(this).attr('id');
		
		if (data_toggle != 'modal') {


            $.ajax({
                    "url": ajax_url + '/alert',
                    "type": "POST",
                    "headers": {
                                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                    },
                    "data": { "vid": current_vid},
                    success: function(data){
                   alert('Attendance reminder has been sent!');
                            //$('#id-html-data-table').DataTable().ajax.reload();
                    },
                    error(error){
                    alert("Error: " + JSON.stringify(error));
                    }
            });
	    		
		}
    	});
       
        $("#id-select-state").select2({
                placeholder: 'Select a state',
                maximumSelectionLength: 1
        });
//get town history

    $('#id-html-data-table').on( 'click', 'a', function () {

      var id = $(this).attr('id');
        var town_details = '/track/attendance/'+id+'/town';
       // alert(town_details);
        $.ajax({
                'url': town_details,
                'type': 'POST',
                 
                'headers': {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                success: function(townHistoryDetails) {
                  console.log(townHistoryDetails);
                        $("#id-table-town_attendance_history tbody").empty(); 
                        $.each(townHistoryDetails, function(i, townHistoryDetails) {
                           var $tr = $("#id-table-town_attendance_history tbody").append(
                                   "<tr>"
                                    +"<td>"+townHistoryDetails.route_name+"</td>"
                                   +"<td>"+townHistoryDetails.loc_text+"</td>"
                                   +"<td>"+townHistoryDetails.att_purpose+"</td>"
                                   +"<td>"+ townHistoryDetails.captured_at+"</td>"
                                    +"<td>"+ townHistoryDetails.beatreason+"</td>"
                                  /*  +"<td>"+ townHistoryDetails.captured_at+"</td>"*/

/*                                   +"<td>"+ (new Date()).toLocaleString()+"</td>"
*/                                 
                                   +"</tr>"
                                   ); 
                        });     
                },
                error(error){
                    //alert("Error: " + JSON.stringify(error));
                }
        });
    });
    



    $("#id-select-state").trigger("change");        
});
</script>

