<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'ApiUserLoginController@login'); 
Route::post('/getAllStageProduction', 'ApiUserLoginController@getAllStageProduction'); 
Route::post('/getMachineofStage', 'ApiUserLoginController@getMachineofStage'); 
Route::post('/getMachineStatus', 'ApiController@getMachineStatus'); 
Route::post('/updateStageStatus', 'ApiController@updateStageStatus'); 






