<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('errors/404', function () {
    return abort(404);
});
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/bprdata', 'BprDataController@index')->name('bprdata');
Route::get('/bprdataCreate', 'BprDataController@create')->name('bprdata.create');
Route::get('bprexportcsv','BprDataController@downloadbpr');


Route::get('/stockdetails', 'StockDetailController@index')->name('stockdetail');
Route::post('/stockdetailsCreate', 'StockDetailController@create')->name('stockdetail.create');
Route::get('/stockdetailsEdit', 'StockDetailController@edit')->name('stockdetail.edit');
Route::get('/stockdetailsoneedit', 'StockDetailController@oneedit')->name('stockdetail.oneedit');
Route::post('/stockdetailsUpdate', 'StockDetailController@update')->name('stockdetail.update');
Route::post('/stockdetailsOneUpdate', 'StockDetailController@oneupdate')->name('stockdetail.oneupdate');
Route::post('/stockdetails', 'StockDetailController@destroy')->name('stockdetail.destroy');
Route::get('stockexportcsv','StockDetailController@downloadstock');
Route::get('/checkemail','StockDetailController@checkEmail');


Route::get('/checkuserEmail','AdminController@checkuserEmail');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::post('/adminUpdate', 'AdminController@update')->name('admin.update');
Route::post('/userStageAllocaction', 'AdminController@UpdateStage')->name('admin.UpdateStage');
Route::post('/adminCreate', 'AdminController@create')->name('admin.create');
Route::get('/getPlantStages', 'AdminController@getPlantStages')->name('admin.getPlantStages');
Route::get('/PlantAllocationCheck', 'AdminController@PlantAllocationCheck')->name('admin.PlantAllocationCheck');
Route::get('/checkStageAndPlant', 'AdminController@checkStageAndPlant')->name('admin.checkStageAndPlant');


Route::get('/checkuom','UomController@checkuom');
Route::get('/uom', 'UomController@index')->name('uom');
Route::post('/uomUpdate', 'UomController@update')->name('uom.update');
Route::post('/uomCreate', 'UomController@create')->name('uom.create');


Route::group(['namespace'=>'Plant'], function(){

	Route::get('/plant','PlantMasterController@index')->name('plantMaster.index');
	Route::post('/store','PlantMasterController@store')->name('plantMaster.store');
	Route::get('/edit','PlantMasterController@edit')->name('plantMaster.edit');
	Route::get('/checkPlantCode','PlantMasterController@checkPlantCode')->name('plantMaster.checkPlantCode');
	Route::post('/update','PlantMasterController@update')->name('plantMaster.update');
	Route::post('/delete','PlantMasterController@delete')->name('plantMaster.delete');

	//PlantStages Route
Route::group(['prefix'=>'plant'], function(){
	Route::get('/stages','PlantStageMasterController@index')->name('plantStageMaster.index');
	Route::post('/store','PlantStageMasterController@store')->name('plantStageMaster.store');
	Route::get('/edit','PlantStageMasterController@edit')->name('plantStageMaster.edit');
	Route::get('/checkPlantCode','PlantStageMasterController@checkPlantCode')->name('plantStageMaster.checkPlantCode');
	Route::post('/update','PlantStageMasterController@update')->name('plantStageMaster.update');
	Route::post('/delete','PlantStageMasterController@delete')->name('plantStageMaster.delete');

	Route::get('/plantAllocation','UserPlantAllocationController@index')->name('plantAllocation.index');
	Route::post('/plantAllocation/store','UserPlantAllocationController@store')->name('plantAllocation.store');
	Route::get('/plantAllocation/edit','UserPlantAllocationController@edit')->name('plantAllocation.edit');
	Route::get('/plantAllocation/checkPlantCode','UserPlantAllocationController@checkPlantCode')->name('plantAllocation.checkPlantCode');
	Route::post('/userStageAllocaction','UserPlantAllocationController@UpdateStage')->name('plantAllocation.UpdateStage');
	Route::get('/getPlantStages','UserPlantAllocationController@getPlantStages')->name('plantAllocation.getPlantStages');




	Route::get('/stageAllocation','UserStageAllocationContoller@index')->name('stageAllocation.index');
	Route::post('/stageAllocation/store','UserStageAllocationContoller@store')->name('stageAllocation.store');
	Route::get('/stageAllocation/edit','UserStageAllocationContoller@edit')->name('stageAllocation.edit');
	Route::get('/stageAllocation/checkstageCode','UserStageAllocationContoller@checkstageCode')->name('stageAllocation.checkstageCode');
	Route::post('/userStageAllocaction','UserStageAllocationContoller@UpdateStage')->name('stageAllocation.UpdateStage');
	Route::post('/stageAllocation/delete','UserStageAllocationContoller@delete')->name('stageAllocation.delete');



});

});