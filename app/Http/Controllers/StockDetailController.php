<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplierStock;
use App\Models\TypeMaster;
use App\Models\UomMaster;
use Auth;
use App\User;
use Carbon\carbon;
use Validator;
use DB;
use Log;

class StockDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_id = Auth::user()->login_id;
        $TYPE = Auth::user()->type;
        if($TYPE == "VENDOR"){

            $stock_list = SupplierStock::where('login_id',$login_id)->orderBy(DB::raw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")'))->get();

            $type = TypeMaster::where('is_active',1)->get();
            $uom_type = UomMaster::where('is_active',1)->get();

            // $user_info = DB::table('tbl_supplier_stock')->where('login_id',$login_id)->select('color', DB::raw('count(color) as total'))->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")'
            // ->groupBy('color')
            // ->get('color');

            $user_info_color_count=SupplierStock::where('login_id',$login_id)->where('is_active',1)->get();

                $Black=0;           
                $Red=0;           
                $Yellow=0;           
                $Green=0;           
                $White=0;           
                foreach ($user_info_color_count as $key => $colors) {
                    switch($colors->color){
                        case "Black":
                        $Black++;
                        break;
                        case "Red":
                        $Red++;
                        break;
                        case "Yellow":
                        $Yellow++;
                        break;
                        case "Green":
                        $Green++;
                        break;
                        case "White":
                        $White++;
                        break;
                    }
                }
        }
        if($TYPE == "BUYER"){

         /*  Select tbl_supplier_stock.id, item,tbl_supplier_stock.type,UOM,
          color,norm,fg_store, wip_stock,
          procure,
          remark,
          tbl_supplier_stock.login_id,
          tbl_supplier_stock.created_date,
          tbl_supplier_stock.is_active,
          DATE_FORMAT(tbl_supplier_stock.updated_date, '%d-%m-%Y') as updated_date,wip_excess_qty,
          name as VENDOR_NAME from tbl_supplier_stock 
          inner join tbl_user_master on tbl_user_master.login_id=tbl_supplier_stock.login_id 
          where tbl_supplier_stock.is_active=1 
          and tbl_supplier_stock.login_id 
          in (select distinct vendor_code from tbl_bpr_data where BUYER_CODE ='$login_id') order by id DESC*/


            $type = TypeMaster::where('is_active',1)->get();
            $uom_type = UomMaster::where('is_active',1)->get();

$stock_list = DB::select( DB::raw("Select tbl_supplier_stock.id, item,tbl_supplier_stock.type,UOM,
          color,norm,fg_store, wip_stock,
          procure,
          remark,
          tbl_supplier_stock.login_id,
          tbl_supplier_stock.created_date,
          tbl_supplier_stock.is_active,
          DATE_FORMAT(tbl_supplier_stock.updated_date, '%d-%m-%Y') as updated_date,wip_excess_qty,
          name as VENDOR_NAME from tbl_supplier_stock 
          inner join tbl_user_master on tbl_user_master.login_id=tbl_supplier_stock.login_id 
          where tbl_supplier_stock.is_active=1 
          and tbl_supplier_stock.login_id 
          in (select distinct vendor_code from tbl_bpr_data where BUYER_CODE ='$login_id')  ORDER BY FIELD(color, 'Black','Red','Yellow','Green','White')") );

dd($stock_list);

          /*$stock_list =  DB::table('tbl_supplier_stock')
            ->join('tbl_user_master','tbl_user_master.login_id','=','tbl_supplier_stock.login_id')
            ->select(DB::raw("select vendor_code from tbl_bpr_data where BUYER_CODE =$login_id"))
            ->where('tbl_supplier_stock.is_active',1)
            ->orderBy('tbl_supplier_stock.id','desc')
            ->get();*/

            // $stock_list = SupplierStock::orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')->where('is_active',1)->get();
                  
            $user_info_color_count=DB::select( DB::raw("Select tbl_supplier_stock.id, item,tbl_supplier_stock.type,UOM,
          color,norm,fg_store, wip_stock,
          procure,
          remark,
          tbl_supplier_stock.login_id,
          tbl_supplier_stock.created_date,
          tbl_supplier_stock.is_active,
          DATE_FORMAT(tbl_supplier_stock.updated_date, '%d-%m-%Y') as updated_date,wip_excess_qty,
          name as VENDOR_NAME from tbl_supplier_stock 
          inner join tbl_user_master on tbl_user_master.login_id=tbl_supplier_stock.login_id 
          where tbl_supplier_stock.is_active=1 
          and tbl_supplier_stock.login_id 
          in (select distinct vendor_code from tbl_bpr_data where BUYER_CODE ='$login_id')  ORDER BY FIELD(color, 'Black','Red','Yellow','Green','White')") );

                $Black=0;           
                $Red=0;           
                $Yellow=0;           
                $Green=0;           
                $White=0;           
                foreach ($user_info_color_count as $key => $colors) {
                    switch($colors->color){
                        case "Black":
                        $Black++;
                        break;
                        case "Red":
                        $Red++;
                        break;
                        case "Yellow":
                        $Yellow++;
                        break;
                        case "Green":
                        $Green++;
                        break;
                        case "White":
                        $White++;
                        break;
                    }
                }




        }

        if($TYPE == "ADMIN"){

            $stock_list = DB::select( DB::raw("Select tbl_supplier_stock.id, item,tbl_supplier_stock.type,UOM,
          color,norm,fg_store, wip_stock,
          procure,
          remark,
          tbl_supplier_stock.login_id,
          tbl_supplier_stock.created_date,
          tbl_supplier_stock.is_active,
          DATE_FORMAT(tbl_supplier_stock.updated_date, '%d-%m-%Y') as updated_date,wip_excess_qty,
          name as VENDOR_NAME from tbl_supplier_stock 
          inner join tbl_user_master on tbl_user_master.login_id=tbl_supplier_stock.login_id 
          where tbl_supplier_stock.is_active=1 
          and tbl_supplier_stock.login_id 
          in (select distinct vendor_code from tbl_bpr_data)  ORDER BY FIELD(color, 'Black','Red','Yellow','Green','White')") );

            $type = TypeMaster::where('is_active',1)->get();
            $uom_type = UomMaster::where('is_active',1)->get();

                     
            $user_info_color_count=DB::select( DB::raw("Select tbl_supplier_stock.id, item,tbl_supplier_stock.type,UOM,
          color,norm,fg_store, wip_stock,
          procure,
          remark,
          tbl_supplier_stock.login_id,
          tbl_supplier_stock.created_date,
          tbl_supplier_stock.is_active,
          DATE_FORMAT(tbl_supplier_stock.updated_date, '%d-%m-%Y') as updated_date,wip_excess_qty,
          name as VENDOR_NAME from tbl_supplier_stock 
          inner join tbl_user_master on tbl_user_master.login_id=tbl_supplier_stock.login_id 
          where tbl_supplier_stock.is_active=1 
          and tbl_supplier_stock.login_id 
          in (select distinct vendor_code from tbl_bpr_data)  ORDER BY FIELD(color, 'Black','Red','Yellow','Green','White')") );

                $Black=0;           
                $Red=0;           
                $Yellow=0;           
                $Green=0;           
                $White=0;           
                foreach ($user_info_color_count as $key => $colors) {
                    switch($colors->color){
                        case "Black":
                        $Black++;
                        break;
                        case "Red":
                        $Red++;
                        break;
                        case "Yellow":
                        $Yellow++;
                        break;
                        case "Green":
                        $Green++;
                        break;
                        case "White":
                        $White++;
                        break;
                    }
                }

            
        }
        
        return view('stockdetail.index',compact('stock_list','type','uom_type','user_info','TYPE','Black','Red','Yellow','Green','White'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $login_id = Auth::user()->login_id;
        $postdata = $request->all();
        

        
        $date = new carbon('now');
        $stock_list_save = new SupplierStock();
        $stock_list_save->login_id= $login_id ;
        $stock_list_save->type=$postdata['type'];
        $stock_list_save->item=$postdata['item'];
        $stock_list_save->UOM=$postdata['UOM'];
        $stock_list_save->norm=$postdata['norm'];
        $stock_list_save->fg_store=$postdata['fg_store'];
        if ($stock_list_save->type=="FG") {
            $stock_list_save->wip_stock=null;
        }else{
            $stock_list_save->wip_stock=$postdata['wip_stock'];
        }
        $stock_list_save->procure=$postdata['procure'];
        $stock_list_save->remark=$postdata['remark'];

        $ECO=(($stock_list_save->fg_store+$stock_list_save->wip_stock)/$stock_list_save->norm)*100;
        
        if (0>=$ECO) {
            $stock_list_save->color = 'Black';
        } 
        elseif($ECO>0 && $ECO <= 33.33){
            $stock_list_save->color = 'Red';
        }
        elseif($ECO>33.33 && $ECO<=66.66){
            $stock_list_save->color = 'Yellow';
        }
        elseif($ECO>66.66 && $ECO<=100){
            $stock_list_save->color = 'Green';
        }
        else{
            $stock_list_save->color = 'White';
        }
        
        $stock_list_save->created_date=$date;
        $stock_list_save->updated_date=$date;
        $stock_list_save->save();
        


        return Back();
    }

    public function checkEmail(Request $request){

        $item = $request->input('items');
        $login_id = Auth::user()->login_id;
            // dd($item);
        $isExists = SupplierStock::where('item',$item)->where('login_id',$login_id)->first();
        if($isExists){
            return response()->json(array("exists" => false));
        }else{
            return response()->json(array("exists" => true));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    public function downloadstock(Request $request)
    {
       try{  

        $login_id = Auth::user()->login_id;
        
        $TYPE = Auth::user()->type;
        if($TYPE == "VENDOR"){
            $stock_list = SupplierStock::where('login_id',$login_id)->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')->get();


            $tot_record_found=0;

            if($stock_list->count()>0){
              $tot_record_found=1;
              
              $CsvData=array('LAST UPDATED ON,ITEM,TYPE,UOM,NORM,FG/STORE STOCK,WIP STOCK,TO PROCURE/ PRODUCE, REMARK, ECO COLOUR');   

              foreach($stock_list as $value){
                
                  if(isset($value->item)){

                      $CsvData[]=date('d/m/Y',strtotime($value->updated_date)).','.$value->item.','.$value->type.','.$value->UOM.','.$value->norm.','.$value->fg_store.','.$value->wip_stock.','.$value->procure.','.$value->remark.','.$value->color;
                  }else{
                    $CsvData[]=date('d/m/Y',strtotime($value->updated_date)).','.$value->item.','.$value->type.','.$value->UOM.','.$value->norm.','.$value->fg_store.','.$value->wip_stock.','.$value->procure.','.$value->remark.','.$value->color;
                }          
            }
        }
        
        $filename=date('Y-m-d').".csv";
        $file_path=base_path().'/'.$filename;   
        $file = fopen($file_path,"w+");
        foreach ($CsvData as $exp_data){
            fputcsv($file,explode(',',$exp_data));
        }

        fclose($file);          

        $headers = ['Content-Type' => 'application/csv'];
        return response()->download($file_path,$filename,$headers );
        
    }

}
catch(\Illuminate\Database\QueryException $e)
{
  
   $request->session()->flash('Not success','!!');
   
}

return Back();
}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function oneedit(Request $request)
    {
        $login_id = Auth::user()->login_id;
        $id = $request->input('id');
        // dd($id);
        $TYPE = Auth::user()->type;
        if($TYPE == "VENDOR"){

            $stock_list = SupplierStock::where('login_id',$login_id)->where('id',$id)->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')->get();

            $type = TypeMaster::where('login_id',$login_id)->where('is_active',1)->get();
            $uom_type = UomMaster::where('login_id',$login_id)->where('is_active',1)->get();

            $user_info = SupplierStock::where('login_id',$login_id)->where('id',$id)->select('color', DB::raw('count(*) as total'))->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')
            ->groupBy('color')
            ->get();
            
        }
        if($TYPE == "BUYER"){
            $type = TypeMaster::where('is_active',1)->get();
            $uom_type = UomMaster::where('is_active',1)->get();
            $stock_list = SupplierStock::orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')->get();
            $user_info = SupplierStock::where('login_id',$login_id)->select('color', DB::raw('count(*) as total'))->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')
            ->groupBy('color')
            ->get();
        }
        return view('stockdetail.oneedit',compact('stock_list','type','uom_type','user_info'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function oneupdate(Request $request)
    {
        $update_id = $request->input('update_id');
        
        $norm = $request->input('norm');
        $fg_store = $request->input('fg_store');
        $wip = $request->input('wip_stock');
        $procure = $request->input('procure');
        $remark = $request->input('remark');
        $colors = $request->input('colors');
        $date = new carbon('now');
        $stock_list_update = SupplierStock::where('id',$update_id)->first();

        //     foreach ($stock_list  as $key => $value) 
        // {
           // $stock_list_update = SupplierStock::find($update_id);

        $stock_list_update->norm=$norm;
        $stock_list_update->fg_store=$fg_store;
        if (!empty($wip)) {
          
         $stock_list_update->wip_stock=$wip;
     }
     $stock_list_update->procure=$procure;
     $stock_list_update->color=$colors;
     $stock_list_update->remark=$remark;
     $stock_list_update->updated_date=$date;

     $stock_list_update->save();
        // }
     return redirect()->route('stockdetail');
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $login_id = Auth::user()->login_id;
        $TYPE = Auth::user()->type;
        if($TYPE == "VENDOR"){

            $stock_list = SupplierStock::where('login_id',$login_id)->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')->get();

            $type = TypeMaster::where('login_id',$login_id)->where('is_active',1)->get();
            $uom_type = UomMaster::where('login_id',$login_id)->where('is_active',1)->get();

            $user_info = SupplierStock::where('login_id',$login_id)->select('color', DB::raw('count(*) as total'))->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')
            ->groupBy('color')
            ->get();
            
        }
        if($TYPE == "BUYER"){
            $type = TypeMaster::where('is_active',1)->get();
            $uom_type = UomMaster::where('is_active',1)->get();
            $stock_list = SupplierStock::orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')->get();
            $user_info = SupplierStock::where('login_id',$login_id)->select('color', DB::raw('count(*) as total'))->orderByRaw('FIELD(color, "Black", "Red", "Yellow", "Green", "White")')
            ->groupBy('color')
            ->get();
        }
        return view('stockdetail.edit',compact('stock_list','type','uom_type','user_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {//dd($request->all());
        $update_id = $request->input('update_id');//dd($update_id);
        //$UOM = $request->input('UOM');
        $norm = $request->input('norm');
        $fg_store = $request->input('fg_store');
        $wip = $request->input('wip_stock');
        $procure = $request->input('procure');
        $remark = $request->input('remark');
        $date = new carbon('now');
        $stock_list = SupplierStock::whereIn('id',$update_id)->get();
        
        foreach ($stock_list  as $key => $value) 
        {//dd($update_id[$key]);
           $stock_list_update = SupplierStock::find($update_id[$key]);//dd($stock_list_update);
           //$stock_list_update->UOM=$UOM[$key];
           $stock_list_update->norm=$norm[$key];
           $stock_list_update->fg_store=$fg_store[$key];
           if (!empty($wip[$key])) {
               # code...
             $stock_list_update->wip_stock=$wip[$key];
         }
         $stock_list_update->procure=$procure[$key];
         $stock_list_update->remark=$remark[$key];
         $stock_list_update->updated_date=$date;

         $stock_list_update->save();
     }
     return redirect()->route('stockdetail');
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        
        SupplierStock::where('id', $id)->delete();
        
        return back();
    }
}
