<?php

namespace App\Http\Controllers\Plant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PlantMaster;
use App\Models\PlantStageMaster;
use Validator;
use Log;
use DB;
use Carbon\Carbon;
class PlantStageMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info('StageInfo PlantStageMasterController@index='.print_r($request->all()));
        $plantInfo=PlantMaster::where('IS_ACTIVE','=','1')->get();
        $getPlantInfo=DB::table('tbl_plant_master')
                    
                     ->join('tbl_production_stages','tbl_plant_master.id','=','tbl_production_stages.plant_id')
                     ->orderBy('tbl_production_stages.order_no')
                      ->select('tbl_plant_master.plant_name','tbl_plant_master.id as PlantId','tbl_production_stages.stage','tbl_production_stages.is_active','tbl_production_stages.id as stageId')->get();
                     
        return view('PlantStageMaster.index',compact('plantInfo','getPlantInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Plant Master Creation Data PlantStageMasterController@store=='.print_r($request->all(),true));
              $formData= $request->all();
              $stage=$request->stage;
              $validator=Validator::make($formData,[
                        'plant_name'=>'required',
                        'stage'=>'required',
              ]);
              if($validator->fails())
              {
                    return redirect()->back()->withErrors($validator)->withInput();
              }
                $StageInfo= new PlantStageMaster();
                $StageInfo->plant_id=$formData['plant_name'];
                $StageInfo->stage=$formData['stage'];

                if($StageInfo->save())
                {
                    return redirect()->back()->with('success_message','Stage is created successfully!!');
                } 
                else
                {
                     return redirect()->back()->with('error_message','Stage is Not created successfully!!');
                }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());
        Log::info('Plant Stage Master Update PlantStageMasterController@update=='.print_r($request->all(),true));

        $updateStage_id=$request->updateStage_id;
        $plant_id=$request->plant_name;
        $stage=$request->stage;
        //$plant_status=$request->IS_ACTIVE;

        $PlantStageExists=PlantStageMaster::where('id',$updateStage_id)->where('stage',$stage)->first(); 
        if(!empty($PlantStageExists))
        {
             $updateStageInfo=PlantStageMaster::where('id',$updateStage_id)
                                       ->update([

                                                   'plant_id'=>$plant_id,
                                                   'is_active'=>$request->is_active,
                                                ]);
        }
        else
        {
             $validator=Validator::make($request->all(),[
                        'plant_name'=>'required',
                        'stage'=>'required|unique:tbl_production_stages,stage',
              ]);
              if($validator->fails())
              {
                    return redirect()->back()->withErrors($validator)->withInput();
              }
                $updateStageInfo=PlantStageMaster::where('id',$updateStage_id)
                                       ->update([

                                                   'plant_id'=>$plant_id,
                                                    'stage'=> $stage,
                                                    'is_active'=>$request->is_active,
                                                ]);
        }
         if(isset($updateStageInfo))
                {
                    return redirect()->back()->with('success_message','Plant Stage Info Updated successfully!!');
                }
                
                else
                {
                     return redirect()->back()->with('error_message','Plant Stage Info Is Not Updated successfully!!');
                }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function delete(Request $request)
    {
        
        Log::info('Plant Stage Master Delete PlantStageMasterController@delete=='.print_r($request->all(),true));

        $delete_id=$request->delete_id;
        $deletePlantStageInfo=PlantStageMaster::where('id',$delete_id)->update(['is_active'=>0]);
        if(!empty( $deletePlantStageInfo))
        {
            return redirect()->back()->with('success_message','Plant Stage Info Deleted successfully!!');
        }
        else
        {
             return redirect()->back()->with('error_message','Plant Stage Info Is Not Deleted successfully!!');
        }
        
   
    }
}
