<?php

namespace App\Http\Controllers\Plant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use DB;
use Carbon\carbon;
use App\Models\PlantMaster;
use App\Models\PlantStageMaster;
use App\Models\UserPlantAllocation;
use App\Models\UserPlantStageAllocation;
use Hash;
use Illuminate\Support\Facades\Log;

class UserPlantAllocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $TYPE = Auth::user()->type;
            $login_id = Auth::user()->login_id;
            if($TYPE == "ADMIN"){
                $users = User::All();

            }
$plantAndStageInfo=DB::table('tbl_user_master')
                                  ->leftJoin('tbl_user_plant_allocation','tbl_user_plant_allocation.user_id','=','tbl_user_master.id')
                                  ->leftJoin('tbl_plant_master','tbl_plant_master.id','=','tbl_user_plant_allocation.plant_id')
                                   ->select('tbl_user_master.id as user_id','tbl_user_plant_allocation.plant_id as plant_id','tbl_user_master.name as name','tbl_user_master.type as type','tbl_plant_master.plant_name','tbl_plant_master.id as plantId','tbl_plant_master.plant_code as plant_code')
                                   ->where('tbl_user_master.type','=',"PRODUCTION")
                                  ->get();
                              //dd($plantAndStageInfo) ;
            $getPlantInfo=PlantMaster::where('IS_ACTIVE',1)->get();

        
        return view('UserPlantAllocation.index',compact('users','getPlantInfo','plantAndStageInfo'));
       
    }

     public function PlantAllocationCheck(Request $request)
    {
          Log::info('Input PlantAllocationCheck UserPlantAllocationController@PlantAllocationCheck='.print_r($request->all(),true));
          $input=$request->all();
          $plant_id=$request->plant_id;  
          $login_id=$input['login_id'];
       
          // Log::info('plant_id PlantAllocationCheck AdminController@PlantAllocationCheck='.print_r($plant_id,true));
          // Log::info('login_id PlantAllocationCheck AdminController@PlantAllocationCheck='.print_r($login_id,true));
        
        
          $user_id=DB::table('tbl_user_master')->where('login_id',$login_id)->first();


         $plantUniqueInfo=DB::table('tbl_user_plant_allocation')->where('user_id',$user_id->id)->count();                              
                                   
           Log::info(' user_id AdminController@PlantAllocationCheck='.print_r($user_id,true));
           Log::info(' plantUniqueInfo AdminController@PlantAllocationCheck='.print_r($plantUniqueInfo,true));
         
                                 if(($plantUniqueInfo)>1){
                return response()->json(array("exists" => false));
        }else{
                return response()->json(array("exists" => true));
        }
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Log::info('Plant Allocation input UserPlantAllocationController@store='.print_r($request->all(),true));
         
         //dd($request->all());

       $input=$request->all();
          DB::beginTransaction();
          try{
                $userInfoUpdate=DB::table('tbl_user_master')
                                ->where('id',$request->user_id)
                                ->update([
                                              'name'=>$request->name,                                         
                                              'type'=>$request->type,
                                        
                                        ]);

                  $plantInfoCheckExists=UserPlantAllocation::where('user_id',$request->user_id)->first();
                  if(!empty($plantInfoCheckExists)){
                     $plantInfoUpdate=UserPlantAllocation::where('user_id',$request->user_id)            
                                 ->update([    

                                                'plant_id'=>$request->plant_name,
                                                'user_id'=>$request->user_id,
                                        ]);
                  }
                  else{
                           $plantInfoCreate= new UserPlantAllocation();
                           $plantInfoCreate->user_id=$request->user_id;
                           $plantInfoCreate->plant_id=$request->plant_name;

                           $plantInfoCreate->save();

                      } 

                       $getPlantStages=$request->stage;

                       //update old stage

                    $updateoldStage=UserPlantStageAllocation::where('user_id',$input['user_id'])->update(["is_active"=>0]);
                 
                     foreach ($getPlantStages as $key => $value) {

                    $StageAllocation= UserPlantStageAllocation::create([

                            'user_id'=>$request->user_id,
                            'stage_id'=>$value,

                    ]);
                    

                         
                    }
                       DB::commit(); 
                             return redirect()->back()->with('success_message','Plant Allotted successfully!!');            
         }
           catch(\Exception $e){
                
          $request->session()->flash('error_message','Something wrong!!');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


   public function getPlantStages(Request $request)
    {
          Log::info('Get Plant stageInfo UserStageAllocationContoller@getPlantStages='.print_r($request->all(),true));
          $plant_id=$request->plant_id;


        $getPlantStages = PlantStageMaster::where('plant_id',$plant_id)->where('is_active','=','1')->orderBy('order_no')->get();
        $this->jsonResponse['success'] = true;
        $this->jsonResponse['data'] = $getPlantStages;
        return response()->json($this->jsonResponse);

    }





    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
