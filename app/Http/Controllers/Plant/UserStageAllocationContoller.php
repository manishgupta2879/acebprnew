<?php

namespace App\Http\Controllers\Plant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use DB;
use Carbon\carbon;
use App\Models\PlantMaster;
use App\Models\PlantStageMaster;
use App\Models\UserPlantAllocation;
use App\Models\UserPlantStageAllocation;
use Hash;
use Illuminate\Support\Facades\Log;
class UserStageAllocationContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $StageInfo=DB::table('tbl_user_master')
                                  ->join('tbl_user_plant_allocation','tbl_user_plant_allocation.user_id','=','tbl_user_master.id')
                                 // ->join('tbl_user_stages_allocation','tbl_user_stages_allocation.user_id','tbl_user_plant_allocation.user_id')
                                  ->join('tbl_plant_master','tbl_plant_master.id','=','tbl_user_plant_allocation.plant_id')
                                  ->where('tbl_user_master.type','=',"PRODUCTION")
                                 // ->join('tbl_production_stages','tbl_production_stages.id','=','tbl_user_stages_allocation.stage_id')
                                  ->groupBy('tbl_user_master.id')
                                  ->selectRaw('tbl_user_master.*,tbl_user_master.id as uid , tbl_plant_master.*,tbl_user_plant_allocation.*,"" as stage')
                                  ->get();

        //Log::info('UserStageAllocation@index -   '.print_r($StageInfo,true));

              foreach($StageInfo as $stage){



                $stageData=DB::table('tbl_user_stages_allocation')
                     ->join('tbl_production_stages','tbl_production_stages.id','=','tbl_user_stages_allocation.stage_id')
                     ->where('tbl_user_stages_allocation.user_id',$stage->uid)
                       ->where('tbl_user_stages_allocation.is_active',1)
                       ->orderBy('tbl_production_stages.order_no')
                     ->pluck('tbl_production_stages.stage')->toArray();


                      //Log::info('UserStageAllocation@index  stageData -   '.print_r($stageData,true));


                     $stage->stage=$stageData;


              }


               Log::info('UserStageAllocation@index -   stage info  - '.print_r($StageInfo,true));

                                
                      $getPlantInfo=PlantMaster::where('IS_ACTIVE',1)->get();           
        return view('UserStageAllocation.index',compact('StageInfo','getPlantInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Log::info('Plant Allocation input UserStageAllocationContoller@store='.print_r($request->all(),true));
         
        $input=$request->all();
        $stage=array();

        if(isset($input['stage'])){


        $stage=$input['stage'];
        }
                



          DB::beginTransaction();
          try{
                  
         $updateoldStage=UserPlantStageAllocation::where('user_id',$input['user_id'])->update(["is_active"=>0]);
        Foreach($stage as $value){


             $getStageOfUser=new UserPlantStageAllocation();
             $getStageOfUser->user_id=$input['user_id'];
             $getStageOfUser->stage_id=$value;
             $getStageOfUser->allocation_date=date('Y-m-d H:i:s');
             $getStageOfUser->save();


        }
        DB::commit(); 
                
         }

      


           catch(\Exception $e){
                
         return redirect()->back()->with('success_message','Some error occure!!');
        }


       
         //dd($request->all());
       
                

                 
                   
                    
         return redirect()->back()->with('success_message','Plant Allotted successfully!!');  
 
    }





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
