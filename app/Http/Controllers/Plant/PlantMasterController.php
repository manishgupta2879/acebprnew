<?php

namespace App\Http\Controllers\Plant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Validator;
use Illuminate\Validation\Rule;
use App\Models\PlantMaster;
use App\Models\PlantStageMaster;
class PlantMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        Log::info('PlantMasterController@index=='.print_r($request->all(),true));
        $plantInfo=PlantMaster::all();
        return view('PlantMaster.index',compact('plantInfo'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
              Log::info('Plant Master Creation Data PlantMasterController@store=='.print_r($request->all(),true));
              $formData= $request->all();
              $plant_code=$request->plant_code;

              $validator=Validator::make($formData,[
                        'plant_name'=>'required',
                        'plant_code'=>'required|unique:tbl_plant_master,plant_code',
              ]);
              if($validator->fails())
              {
                    return redirect()->back()->withErrors($validator)->withInput();
              }
                $plantInfo= PlantMaster::create($formData);
                if(isset($plantInfo))
                {
                    return redirect()->back()->with('success_message','Plant is created successfully!!');
                } 
                else
                {
                     return redirect()->back()->with('error_message','Plant is Not created successfully!!');
                }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }




 public function checkPlantCode(Request $request){

        $plant_code = $request->input('plant_code');
        
        $isExists = PlantMaster::where('plant_code',$plant_code)->first();
        if(!empty($isExists)){
                return response()->json(array("exists" => false));
        }else{
                return response()->json(array("exists" => true));
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Log::info('Plant Master Update PlantMasterController@update=='.print_r($request->all(),true));

        $update_plant_id=$request->plant_id;       
        $plant_name=$request->plant_name;
        $plant_code=$request->plant_code;
        $plant_status=$request->IS_ACTIVE;

        $PlantExists=PlantMaster::where('id',$update_plant_id)->where('plant_code',$plant_code)->first(); 
        $StageExists=PlantStageMaster::where('plant_id',$update_plant_id)->count();

      for ($i=0;$i<$StageExists;$i++) {
          $StageInfo=PlantStageMaster::where('plant_id',$update_plant_id)->update([                                                  
                                                  'is_active'=> $plant_status,
                                                ]);                                      
      }
        if(!empty($PlantExists)){
             $updatePlantInfo=PlantMaster::where('id',$update_plant_id)
                                       ->update([
                                                   'plant_name'=>$plant_name,
                                                   'IS_ACTIVE'=> $plant_status,
                                                ]);
           
         
        }
        else{
             $validator=Validator::make($request->all(),[
                        'plant_name'=>'required',
                        'plant_code'=>'required|unique:tbl_plant_master,plant_code',
              ]);
              if($validator->fails())
              {
                    return redirect()->back()->withErrors($validator)->withInput();
              }
                $updatePlantInfo=PlantMaster::where('id',$update_plant_id)
                                       ->update([
                                                   'plant_name'=>$plant_name,
                                                   'IS_ACTIVE'=> $plant_status,
                                                ]);         
        }
          $StageExists=PlantStageMaster::where('plant_id',$update_plant_id)->count();

      for ($i=0;$i<$StageExists;$i++) {

          $StageInfo=PlantStageMaster::where('plant_id',$update_plant_id)->update([                                                  
                                                  'is_active'=> $plant_status,
                                                ]);                                      
      }
         if(isset($updatePlantInfo))
                {
                    return redirect()->back()->with('success_message','Plant Info Updated successfully!!');
                }
                
                else
                {
                     return redirect()->back()->with('error_message','Plant Info Is Not Updated successfully!!');
                }
        
        }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $delete_id=$request->delete_id;
        $deletePlantInfo=PlantMaster::where('id',$delete_id)->update(['IS_ACTIVE'=>0]);
        $deletePlantStage=PlantStageMaster::select('plant_id')->where('plant_id',$delete_id)->get();
        foreach ($deletePlantStage as $key => $value) {
           PlantStageMaster::where('plant_id',$value)->update(['is_active'=>0]);
        }

        if(!empty( $deletePlantInfo))
        {
            return redirect()->back()->with('error_message','Plant Info Deleted successfully!!');
        }
        else
        {
             return redirect()->back()->with('error_message','Plant Info Is Not Deleted successfully!!');
        }
        
    }
}
