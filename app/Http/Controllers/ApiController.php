<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Auth ;
use Response;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Exceptions\HttpResponseException;
use stdClass;
use App\helpers\verifyTockenHealper;
use DateTime;

class ApiController extends Controller
{
    //

    public function getMachineStatus(Request $request){


    	 Log::info('ApiController@getMachineStatus input - '.print_r($request->all(),true));
		     $input=$request->all();
		     $machine_id=$input['machine_id'];
		     $plant_id=$input['plant_id'];
		     $responseObj=new stdClass();
		     $token=$input['user_token'];
		     $user = (new verifyTockenHealper())->verifyToken($token);


		     Log::info('ApiController@getMachineStatus user '.print_r($user,true));
		    // Log::info('ApiUserLoginController@getAllStageProduction plant '.print_r($user->plant_id,true));
		     if(!$user){

		     	   $responseObj->status_code = 1001;
                   $responseObj->status_msg = 'Login Fail';
                   $responseObj->data = [];



		     }else{


		     	  $machine=DB::table('tbl_machine_master')
	 		     	  ->where('tbl_machine_master.id',$machine_id)
		     	      ->where('tbl_machine_master.plant_id',$plant_id)
			     	  ->select('tbl_machine_master.*')
			     	  ->first();
			      	Log::info('machine details id  - '.print_r($machine,true));

			      	 if(!$machine){

					     	   $responseObj->status_code = 1001;
			                   $responseObj->status_msg = 'Machine details not found! ';
			                   $responseObj->data = [];

			                   return Response::json($responseObj,200);    



		              }
	  

			     $stage=DB::table('tbl_production_stages')
			       

			         ->leftJoin('tbl_machine_stages', function($join) use ($machine_id){
				            $join->on('tbl_machine_stages.stage_id','=','tbl_production_stages.id')
				           ->where('tbl_machine_stages.id',$machine_id);
				          })
	 		     	  
		     	      ->where('tbl_production_stages.plant_id',$plant_id)
		     	      ->where('tbl_production_stages.is_active',1)

		     	      ->orderBy('order_no')
			     	  ->selectRaw('tbl_production_stages.*,
			     	  	IFNULL(tbl_machine_stages.stage_date,"") as stage_date,
			     	  	IFNULL(tbl_machine_stages.is_active,0) as state_status')
			     	  ->get();	

			     	   if($stage->isEmpty()){

					     	   $responseObj->status_code = 1001;
			                   $responseObj->status_msg = 'Stage details not found! ';
			                   $responseObj->data = [];

			                   return Response::json($responseObj,200);    



		              } 


			    $getuserStageAcess=DB::table('tbl_user_stages_allocation')
			                        ->where('user_id',$user->id)
			                        ->where('is_active',1)
			                        ->pluck('stage_id') ;  





			     Log::info('getuserStageAcess  -  '.print_r($getuserStageAcess,true)); 


			   

			     	  foreach($stage as $stagevalue){

			     	  	$stagevalue->user_access="No";
			     	  	//update user access
			     	  	 foreach($getuserStageAcess as $stageId){

			     	  	 	if($stagevalue->id==$stageId){

	                             $stagevalue->user_access="Yes";

			     	  	 	}

			     	  	 }
                 //update edit acces
	     	     	if($stagevalue->stage_date){

                                $stagevalue->edit_access="No";

			     	  	}else{
                               $stagevalue->edit_access="Yes";

			     	  	}


			     	  	// get  machine last stage
			     	  	$final_stage=$machine->final_stage+1;
			     	  	Log::info('machine details  final_stage - '.print_r($final_stage,true));

			     	  	if($stagevalue->order_no==$final_stage){

                                $stagevalue->edit_access="Yes";

			     	  	}else{
                               $stagevalue->edit_access="No";

			     	  	}



			     	  }

		     	  $responseObj->status_code = 1000;
                  $responseObj->status_msg = 'Success';
                  $responseObj->data=new stdClass();
                  $responseObj->data->machine_details = $machine;
                  $responseObj->data->stage_details = $stage;

		     }

		   return Response::json($responseObj , 200);    




    }



        public function updateStageStatus(Request $request){

        	$input=$request->all();
        	$responseObj=new stdClass();
            Log::info('ApiController@updateStageStatus input - '.print_r($input,true));
             $token=$input['user_token'];
		     $user = (new verifyTockenHealper())->verifyToken($token);


		     Log::info('ApiController@getMachineStatus user '.print_r($user,true));
		    // Log::info('ApiUserLoginController@getAllStageProduction plant '.print_r($user->plant_id,true));
		     if(!$user){

		     	   $responseObj->status_code = 1001;
                   $responseObj->status_msg = 'Login Fail';
                    return Response::json($responseObj,200);    



		     }else{


             $getuserStageAcess=DB::table('tbl_user_stages_allocation')
			                        ->where('user_id',$user->id)
			                        ->where('is_active',1)
			                        ->pluck('stage_id')->toArray() ;  


            Log::info('getuserStageAcess  - '.print_r($getuserStageAcess,true));
			   //chaeck stage access
			   
			   if (!in_array($input['stage_id'], $getuserStageAcess)){


			   	   $responseObj->status_code = 1001;
                   $responseObj->status_msg = 'Acess Denied!';
                   return Response::json($responseObj,200);    



			   }                     

		     }


            $stageDate=$input['stage_date'];
                  if(!$stageDate){

                $responseObj->status_msg="Stage Date Not Exist!";
		     	$responseObj->status_code=1001;
		        return Response::json($responseObj,200);    

                  }

             $stageDate=DateTime::createFromFormat('d-m-Y H:i:s', $stageDate)->format('Y-m-d H:i:s');
             
             Log::info('ApiController@updateStageStatus stageDate - '.print_r($stageDate,true));      

           /* $checkStageStatus=DB::table('tbl_machine_stages')->insert([
			    ['email' => 'taylor@example.com', 'votes' => 0],
			    ['email' => 'dayle@example.com', 'votes' => 0]
			]);*/

             $checkStageStatus=DB::table('tbl_machine_stages')
		             ->where('machine_id',$input['machine_id'])
		             ->where('stage_id',$input['stage_id'])
		               ->where('is_active',1)
		             ->first();

		     Log::info('ApiController@updateStageStatus checkStageStatus - '.print_r($checkStageStatus,true));
		     
		     if($checkStageStatus){

		     	$responseObj->status_msg="Stage already update";
		     	$responseObj->status_code=1001;
		        return Response::json($responseObj,200);    


		     } else{
		     	Log::info('ApiController@updateStageStatus ready to record create !!!!!!!');

		     	DB::beginTransaction();
                 try{


		     	  $checkStageStatus=DB::table('tbl_machine_stages')->insert([
                       ['machine_id'=>$input['machine_id'],
                      'stage_id'=>$input['stage_id'],
                       'stage_date'=>$stageDate]

		     	  ]);

//update old status of machine

		     	  $checkStageStatus=DB::table('tbl_machine_stages')
		     	  ->where('machine_id',$input['machine_id'])
		     	  ->where('stage_id','!=',$input['stage_id'])
		     	  ->update([
                       'is_active'=>0

		     	  ]);



                 $responseObj->status_msg="Record Create Successfully!";
		     	 $responseObj->status_code=1000;



		     	 DB::commit();
           
         } catch (\Exception $e) {
            DB::rollback();
               $responseObj->status_msg="Some error occure!";
		     	$responseObj->status_code=1001;
		        return Response::json($responseObj,200);    
            }



		     }  
		     return Response::json($responseObj,200);     
   
        	
        	





        }
}
