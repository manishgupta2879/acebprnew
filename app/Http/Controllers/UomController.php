<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\UomMaster;
use DB;
use Carbon\carbon;

class UomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            $TYPE = Auth::user()->type;
            $login_id = Auth::user()->login_id;
            if($TYPE == "ADMIN"){
                $Uom = UomMaster::All();

            }
         // dd($users);
        return view('uom.index',compact('Uom'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
            $date = new carbon('now');
            $id = Auth::user()->login_id;
            $user_insert = new UomMaster();
            $user_insert->name= $request->input('name');
            $user_insert->login_id= $id;
            // $user_insert->updated_at= $date;

            $user_insert->save();
 


       return Back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function checkuom(Request $request){

        $name = $request->input('names');
        
        $isExists = UomMaster::where('name',$name)->first();
        if(!empty($isExists)){
                return response()->json(array("exists" => false));
        }else{
                return response()->json(array("exists" => true));
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{

            $id = $request->input('update_id');
            $name = $request->input('name');
            $array = array_combine($id, $name);
     
            $uom_list = UomMaster::whereIn('id',$id)->get();
          
            foreach ($array  as $key => $value) 
            {
               $uom_update = UomMaster::find($key);

               if($uom_update->name!=$value)
               {
                $checkName= UomMaster::where('name',$value)->first();
                // dd($uom_update->name, $value, $checkName);
                if(!empty($checkName)){

                    $request->session()->flash('Danger','Name Available!!');

                }else{
                    $uom_update->name=$value;
                    $uom_update->save();
                     $request->session()->flash('success','Update Successfully !!');
                }
               // $request->session()->flash('success','No Update !!');
               }
            }
        }
          catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return redirect()->route('uom');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
