<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use DB;
use Carbon\carbon;
use App\Models\PlantMaster;
use App\Models\PlantStageMaster;
use App\Models\UserPlantAllocation;
use App\Models\UserPlantStageAllocation;
use Hash;
use stdClass;
use Response;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
  public $jsonResponse = ['success' => false, 'message' => 'Sorry!, unable to process your request' , 'data' => ''];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           
            $TYPE = Auth::user()->type;
            $login_id = Auth::user()->login_id;
            if($TYPE == "ADMIN"){
                $users = User::All();

            }
           
           $plantAndStageInfo=DB::table('tbl_user_master')
                                  ->join('tbl_user_plant_allocation','tbl_user_plant_allocation.user_id','=','tbl_user_master.id')
                                  ->join('tbl_user_stages_allocation','tbl_user_stages_allocation.user_id','=','tbl_user_plant_allocation.user_id')
                                  ->get();
                                 
                              
            $getPlantInfo=PlantMaster::where('IS_ACTIVE',1)->get();

        
        return view('admin.index',compact('users','getPlantInfo','plantInfo','plantAndStageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {     
       
           Log::info('user Create AdminController@create='.print_r($request->all(),true));
         
            DB::beginTransaction();
            try
            {
                    $date = new carbon('now');
                    $user_insert = new User();
                    $user_insert->name= $request->input('name');
                    $user_insert->login_id= $request->input('login_id');
                    $user_insert->email= $request->input('login_id');
                    $user_insert->type= $request->input('type');
                    $user_insert->mobile= $request->input('mobile');
                    $user_insert->password_show= $request->input('password');
                    $user_insert->password= bcrypt($request->input('password'));

                    $user_insert->save();

                    $UserPlantAllocation=new UserPlantAllocation();
                    $UserPlantAllocation->user_id= $user_insert->id;
                    $UserPlantAllocation->plant_id=$request->plant_name;
                    $UserPlantAllocation->allocation_date=new carbon('now');
                    $UserPlantAllocation->save();

                    $getPlantStages=$request->stage;
                    foreach ($getPlantStages as $key => $value) {

                    $StageAllocation=new UserPlantStageAllocation();
                    $StageAllocation->user_id=$user_insert->id;
                    $StageAllocation->stage_id=$value;
                    $StageAllocation->allocation_date=new carbon('now');

                    $StageAllocation->save();
                         
                    }
                      DB::commit();
                         return redirect()->back()->with('success_message','User created successfully!!');
           
            }catch(\Exception $e )
             {
                    DB::rollback();
                    return redirect()->back()->with('error_message','User Not created successfully!!');
             }     
    }


    public function getPlantStages(Request $request)
    {
          Log::info('Get Plant stageInfo AdminController@getPlantStages='.print_r($request->all(),true));
          $input=$request->all();
          $uid=$input['user_id'];
          $responseObj=new stdClass();
          $plant_detail=DB::table('tbl_user_plant_allocation')
          ->join('tbl_plant_master','tbl_plant_master.id','=','tbl_user_plant_allocation.plant_id')
          ->where('user_id',$uid)
           ->where('tbl_user_plant_allocation.is_active',1)
             ->where('tbl_plant_master.is_active',1)
          ->select('tbl_plant_master.id as plant_id','tbl_plant_master.plant_name')
          ->first();
          if($plant_detail){
            $plant_id=$plant_detail->plant_id;


              //get user state allocation
            $userstage=DB::table('tbl_user_stages_allocation')
            ->join('tbl_production_stages','tbl_production_stages.id','=','tbl_user_stages_allocation.stage_id')
            ->where('user_id',$uid)
            ->where('tbl_user_stages_allocation.is_active',1)
            ->where('tbl_production_stages.is_active',1)
            ->get();

               Log::info('Get Plant stageInfo AdminController@userstage='.print_r($userstage,true));




        
            // get plant state

              $plantstage=DB::table('tbl_production_stages')
           // ->join('tbl_production_stages','tbl_production_stages.id','=','tbl_user_stages_allocation.stage_id')
            ->where('plant_id',$plant_id)
            //->where('tbl_user_stages_allocation.is_active',1)
            ->where('tbl_production_stages.is_active',1)
            ->selectRaw('tbl_production_stages.*,"0" as status')
            ->get();

            foreach($plantstage as $plantvalue){

                 foreach($userstage as $uservalue){

                    if($plantvalue->stage==$uservalue->stage){

                        $plantvalue->status="1";

                    }



            }



            }

             Log::info('Get Plant stageInfo AdminController@plantstage='.print_r($plantstage,true));

            $responseObj->plantstage=$plantstage;
            $responseObj->userstage=$userstage;
            $responseObj->plant_name=$plant_detail->plant_name;
            



          }else{

            $responseObj->plantstage=[];
            $responseObj->userstage=[];



          }

          log::info('responseObj   '.print_r($responseObj,true));

       return Response::json($responseObj,200);




      /*  $getPlantStages = PlantStageMaster::where('plant_id',$plant_id)->where('is_active','=','1')->get();
        $this->jsonResponse['success'] = true;
        $this->jsonResponse['data'] = $getPlantStages;
        return response()->json($this->jsonResponse);*/

    }



     public function PlantAllocationCheck(Request $request)
    {
          Log::info('Input PlantAllocationCheck AdminController@PlantAllocationCheck='.print_r($request->all(),true));
          $input=$request->all();
          $plant_id=$request->plant_id;  
          $login_id=$input['login_id'];    
          $user_id=DB::table('tbl_user_master')->where('login_id',$login_id)->first();
          $plantUniqueInfo=DB::table('tbl_user_plant_allocation')->where('user_id',$user_id->id)->count();                             
                                   
           Log::info(' user_id AdminController@PlantAllocationCheck='.print_r($user_id,true));
           Log::info(' plantUniqueInfo AdminController@PlantAllocationCheck='.print_r($plantUniqueInfo,true));
         
                                 if(($plantUniqueInfo)>1){
                return response()->json(array("exists" => false));
        }else{
                return response()->json(array("exists" => true));
        }
       

    }



public function checkStageAndPlant(Request $request)
{
       Log::info('Input PlantAllocationCheck AdminController@checkStageAndPlant='.print_r($request->all(),true));

       $user_id=$request->user_id;
        Log::info('plantExists AdminController@checkStageAndPlant='.print_r($user_id,true));
       $plantExists=UserPlantAllocation::where('user_id',$user_id)->first();
       $stageExists=UserPlantStageAllocation::where('user_id',$user_id)->get();
      Log::info('plantExists AdminController@checkStageAndPlant=');
      Log::info('stageExists AdminController@checkStageAndPlant');
    
      $final_data=array();
        $final_data['plantExists']=$plantExists;
        $final_data['stageExists']=$stageExists;
         

       if(empty( $plantExists && $stageExists))
       {
                return response()->json($this->jsonResponse);
       }
       else{
        $this->jsonResponse['success']=true;
                $this->jsonResponse['message']='Successfull get data';
                $this->jsonResponse['data']=$final_data;


                Log::info('stageExists AdminController@checkStageAndPlant'.print_r($final_data,true));
                return response()->json($this->jsonResponse);
               
       }

}






    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function checkuserEmail(Request $request){

        $login_id = $request->input('login_ids');
        
        $isExists = User::where('login_id',$login_id)->first();
        if(!empty($isExists)){
                return response()->json(array("exists" => false));
        }else{
                return response()->json(array("exists" => true));
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
             Log::info('user update AdminController@update='.print_r($request->all(),true));
             


        try{

            $update_id = $request->input('update_id');
            $password = $request->input('password');
     
            $user_list = User::whereIn('id',$update_id)->get();
          
            foreach ($user_list  as $key => $value) 
            {
               $user_update = User::find($update_id[$key]);
               $user_update->password=$password[$key];
               $user_update->confirm_password=Hash::make($password[$key]);
               $user_update->save();
            }
            $request->session()->flash('success','Update Successfully !!');
        }
          catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return redirect()->route('admin');
    }


    public function UpdateStage(Request $request)
    {
          


    }






    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
