<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use DB;
use App\Models\BprData;
// use App\Models\suplierStock;


class BprDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->input('from_date'))) {
            $color = $request->input('color');
            $date = $request->input('from_date');
            $date1 = str_replace('/', '-', $date);
            $from_date = date('Y-m-d', strtotime($date1));
            $TYPE = Auth::user()->type;
            $login_id = Auth::user()->login_id;
            if($TYPE == "VENDOR"){
                $bpr_list = BprData::where('VENDOR_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->get();
                 if(!empty($color)){
                    $bpr_list = BprData::where('VENDOR_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->where('ECO_PENE_COLOUR',$color)->get();
                 }

                $bpr_list1 = BprData::where('VENDOR_CODE',$login_id)->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->count('ECO_PENE_COLOUR');
                 
                $user_info_color_count = BprData::where('VENDOR_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->get();

                $Black=0;           
                $Red=0;           
                $Yellow=0;           
                $Green=0;           
                $White=0;           
                foreach ($user_info_color_count as $key => $colors) {
                    switch($colors->ECO_PENE_COLOUR){
                        case "Black":
                        $Black++;
                        break;
                        case "Red":
                        $Red++;
                        break;
                        case "Yellow":
                        $Yellow++;
                        break;
                        case "Green":
                        $Green++;
                        break;
                        case "White":
                        $White++;
                        break;
                    }
                }

            }
// dd($Red);

            if($TYPE == "BUYER"){
                $bpr_list = BprData::where('BUYER_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->get();//dd($bpr_list);

                $bpr_list1 = BprData::where('BUYER_CODE',$login_id)->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->count('ECO_PENE_COLOUR');
             

                      
                $user_info_color_count = BprData::where('BUYER_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->get();

                $Black=0;           
                $Red=0;           
                $Yellow=0;           
                $Green=0;           
                $White=0;           
                foreach ($user_info_color_count as $key => $colors) {
                    switch($colors->ECO_PENE_COLOUR){
                        case "Black":
                        $Black++;
                        break;
                        case "Red":
                        $Red++;
                        break;
                        case "Yellow":
                        $Yellow++;
                        break;
                        case "Green":
                        $Green++;
                        break;
                        case "White":
                        $White++;
                        break;
                    }
                }
            
            }

            if($TYPE == "ADMIN"){
                $bpr_list = DB::table('tbl_bpr_data')->where('BPR_DATE','like','%'.$from_date.'%')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->get();

                $bpr_list1 = DB::table('tbl_bpr_data')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green", "White")')->count('ECO_PENE_COLOUR');
                 // dd($bpr_list);
                      
                $user_info_color_count = DB::table('tbl_bpr_data')->where('BPR_DATE','like','%'.$from_date.'%')->get();

                $Black=0;           
                $Red=0;           
                $Yellow=0;           
                $Green=0;           
                $White=0;           
                foreach ($user_info_color_count as $key => $colors) {
                    switch($colors->ECO_PENE_COLOUR){
                        case "Black":
                        $Black++;
                        break;
                        case "Red":
                        $Red++;
                        break;
                        case "Yellow":
                        $Yellow++;
                        break;
                        case "Green":
                        $Green++;
                        break;
                        case "White":
                        $White++;
                        break;
                    }
                }
            }
           

            return view('bprdata.index',compact('bpr_list','bpr_list1','user_info','from_date','Black','Red','Yellow','Green','White'));
        }
        
        return view('bprdata.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bprdata.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function downloadbpr(Request $request)
    {
         try{  

             $login_id = Auth::user()->login_id;
             $from_date = $request->input('from_date');
           
                $TYPE = Auth::user()->type;

                if($TYPE == "VENDOR"){

                    $bpr_list = BprData::where('VENDOR_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green")')->get();

           $tot_record_found=0;

          if($bpr_list->count()>0){
              $tot_record_found=1;
               
               $CsvData=array('BPR DATE,INVENTORY LOCATION,ITEM CODE,DESCRIPTION,UOM,ECO PERCENT,BUYER NAME,ECO PENE COLOUR,QUANTITY');   

              foreach($bpr_list as $value){
                    
                  if(isset($value->VENDOR_CODE)){

                              $CsvData[]=date('d/m/Y',strtotime($value->BPR_DATE)).','.$value->INVENTORY_LOCATION.','.$value->ITEM_CODE.','.$value->DESCRIPTION.','.$value->UOM.','.$value->ECO_PERCENT.','.$value->BUYER_NAME.','.$value->ECO_PENE_COLOUR.','.$value->QUANTITY;
                     }else{
                              $CsvData[]=date('d/m/Y',strtotime($value->BPR_DATE)).','.$value->INVENTORY_LOCATION.','.$value->ITEM_CODE.','.$value->DESCRIPTION.','.$value->UOM.','.$value->ECO_PERCENT.','.$value->VENDOR_NAME.','.$value->VENDOR_CODE.','.$value->ECO_PENE_COLOUR;
                    }          
              }
            }
        }
              
                if($TYPE == "BUYER"){

                    $bpr_list = BprData::where('BUYER_CODE',$login_id)->where('BPR_DATE','like','%'.$from_date.'%')->orderByRaw('FIELD(ECO_PENE_COLOUR, "Black", "Red", "Yellow", "Green")')->get();

           $tot_record_found=0;
// dd($bpr_list);
          if($bpr_list->count()>0){
              $tot_record_found=1;
               
               $CsvData=array('BPR DATE,INVENTORY LOCATION,ITEM CODE,DESCRIPTION,UOM,ECO PERCENT,VENDOR NAME,ECO PENE COLOUR,QUANTITY');    

              //$CsvData=array('Item Name,Price,Effective Date');         

              foreach($bpr_list as $value){
                    
                  if(isset($value->VENDOR_CODE)){

                              $CsvData[]=date('d/m/Y',strtotime($value->BPR_DATE)).','.$value->INVENTORY_LOCATION.','.$value->ITEM_CODE.','.$value->DESCRIPTION.','.$value->UOM.','.$value->ECO_PERCENT.','.$value->VENDOR_NAME.','.$value->ECO_PENE_COLOUR.','.$value->QUANTITY;
                     }else{
                              $CsvData[]=date('d/m/Y',strtotime($value->BPR_DATE)).','.$value->INVENTORY_LOCATION.','.$value->ITEM_CODE.','.$value->DESCRIPTION.','.$value->UOM.','.$value->ECO_PERCENT.','.$value->VENDOR_NAME.','.$value->ECO_PENE_COLOUR.','.$value->QUANTITY;
                    }          
              }

            }
        }
              $filename=date('Y-m-d').".csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
            

     }
    catch(\Illuminate\Database\QueryException $e)
      {
                      
                     $request->session()->flash('Not success','!!');
                      
      }

        return Back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
