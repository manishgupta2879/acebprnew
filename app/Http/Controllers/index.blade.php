@extends('layout.app')
@section('pageTitle')

&nbsp;<i class="fa fa-dashboard">&nbsp;&nbsp;PASSWORD UPDATE</i>
   
@endsection
@section('content')


<div class="col-md-12">
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="padding: 7px 12px;">
   <span class="glyphicon glyphicon-plus"></span>
 </button>
 <br>
 <br>
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">Plant Allocation To User</h3>

      </div>
      <div class="modal-body">
        <form autocomplete="off" action="{{ route('admin.create') }}" method="post" >
          {{ csrf_field() }}
          
          
          <div class="form-group">
            <div class="row">
              <label class="control-label col-sm-3" for="name">NAME: <span style="color:#f70b0b;">*</span></label>
              <div class="col-sm-9">
               <input type="text" class="form-control" required="required" name="name" id="name" >
             </div>

           </div>
         </div>
         
         <div class="form-group">
          <div class="row">
            <label class="control-label col-sm-3" for="login_id">USER ID:<span style="color:#f70b0b;">*</span> </label>
            <div class="col-sm-9" id="append">
             <input type="text" onkeyup="duplicateEmail(this)" class="form-control" required="required" name="login_id" id="login_id" >
           </div>
         </div>
       </div>
       <div class="form-group">
        <div class="row">
          <label class="control-label col-sm-3" for="type">USER TYPE:<span style="color:#f70b0b;">*</span> </label>
          <div class="col-sm-9">
           <select class="form-control" required="required" id="type" name="type" >
             <option value="">--Select Type--</option>
             <option value="VENDOR">VENDOR</option>
             <option value="BUYER">BUYER</option>
             <option value="ADMIN">ADMIN</option>
             
           </select>
         </div>
       </div>
     </div>
      <div class="form-group">
        <div class="row">
          <label class="control-label col-sm-3" for="type">Plant Name:<span style="color:#f70b0b;">*</span> </label>
          <div class="col-sm-9">
           <select class="form-control" required="required" id="plant_name" name="plant_name" >
             <option value="">--Select Type--</option> 
             @foreach($getPlantInfo as $value)
             <option value="{{$value->id}}">{{$value->plant_name}}</option> 
             @endforeach
           </select>
         </div>
       </div>
     </div>
     <div class="form-group">
      <div class="row">
      <label class="control-label col-sm-3" for="type"">Plant Stages: <span class="required" style="color:#f70b0b;">*</span></label>
        <div class="col-sm-9">
          <select class="form-control tags" style="width: 100%; color: black;"  id="stage" name="stage[]" multiple>

          </select>                         
       </div>
    </div>
    </div>
  
     <div class="form-group">
       <div class="row">
        <label class="control-label col-sm-3" for="mobile">MOBILE:<span style="color:#f70b0b;">*</span> </label>
        <div class="col-sm-9">
         <input type="text"  class="form-control " required="" id="mobile" name="mobile">
       </div>
     </div>
   </div>
   <div class="form-group">
     <div class="row">
      <label class="control-label col-sm-3" for="password">PASSWORD: <span style="color:#f70b0b;">*</span></label>
      <div class="col-sm-9">
       <input type="password"  class="form-control" required=""  name="password">
     </div>
   </div>
 </div>
 <div class="form-group">
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary" id="submituser">Submit</button>
  </div>
</div>

</form>
</div>

</div>
</div>
</div>
</div>



@endsection


@section('script')





@endsection