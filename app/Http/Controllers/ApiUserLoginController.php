<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Auth ;
use Response;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Exceptions\HttpResponseException;
use stdClass;
use App\helpers\verifyTockenHealper;

class ApiUserLoginController extends Controller
{


	
    public function login(Request $request){
    	Log::info('ApiUserLoginController@login  input - '.print_r($request->all(),true));
    	  $input=$request->all();
    	  $responseObj = new stdClass();
    	  $fcm_id=0;
    	 if( isset($input['pushtoken'])){

    	  	$fcm_id=$input['pushtoken'];
    	  }

    	  if(!$fcm_id&&$fcm_id==0){

    	  	  $responseObj->status_code = 1001;
	          $responseObj->status_msg = 'Token not found.';
			  $responseObj->data = [];


    	  	return Response::json($responseObj , 200);


    	  }



        if(Auth::attempt(Input::only('login_id','password'))){
			Log::info('Authenticated Successfully');

			$authUser=Auth::user();
			//Log::info('ApiUserLoginController@login authUser - '.print_r($authUser->get(),true));

			$authUser=$authUser->where('type',"PRODUCTION")->first();

			//Log::info('ApiUserLoginController@login authUser after where - '.print_r($authUser->get(),true));


		if ($authUser) {

			

			$authUser->user_token = str_random(60);
			$authUser->pushtoken = $fcm_id;

			$authUser->save();

			$plant_id=DB::table('tbl_user_plant_allocation')->where('user_id',$authUser->id)->value('plant_id');
			Log::info('ApiUserLoginController@login plant_id - '.print_r($plant_id,true));


			Foreach($authUser as $user){

              $authUser->plant_id=$plant_id;

			}

			  $responseObj->status_code = 1000;
	          $responseObj->status_msg = 'Success';
			  $responseObj->data = $authUser;

			if(!$plant_id){
              $responseObj->status_code = 1001;
	          $responseObj->status_msg = 'Plant detail not exist.';
			  $responseObj->data = $authUser;


			}

		    

		}else{
                  $responseObj->status_code = 1001;
                  $responseObj->status_msg = 'Not a Production User';
                  $responseObj->data = [];


		}
        Log::info('ApiUserLoginController@login  authUser - '.print_r($authUser,true));

		}else{
                  $responseObj->status_code = 1001;
                  $responseObj->status_msg = 'Login Fail';
                  $responseObj->data = [];


		}


		 return Response::json($responseObj , 200);
   

    }

    public function getAllStageProduction(Request $request){

		     Log::info('ApiUserLoginController@getAllStageProduction input - '.print_r($request->all(),true));
		     $input=$request->all();
		     $plant_id=$input['plant_id'];
		     $responseObj=new stdClass();
		     $token=$input['user_token'];
		     $user = (new verifyTockenHealper())->verifyToken($token);


		     Log::info('ApiUserLoginController@getAllStageProduction user '.print_r($user,true));
		    // Log::info('ApiUserLoginController@getAllStageProduction plant '.print_r($user->plant_id,true));
		     if(!$user){

		     	  $responseObj->status_code = 1001;
                  $responseObj->status_msg = 'Login Fail';
                   $responseObj->data = [];



		     }else{

		     	$stage=DB::table('tbl_production_stages')
		     	->where('plant_id',$plant_id)
		     	->where('is_active',1)
		     	->orderBy('order_no')->get();
		     	  $responseObj->status_code = 1000;
                  $responseObj->status_msg = 'Success';
                   $responseObj->data = $stage;
		     }

		      return Response::json($responseObj , 200);
   

    }



   public function getMachineofStage(Request $request){

   	  Log::info('ApiUserLoginController@getMachineofStage input - '.print_r($request->all(),true));
		     $input=$request->all();
		     $stage_id=$input['stage_id'];
		     $plant_id=$input['plant_id'];
		     $responseObj=new stdClass();
		     $token=$input['user_token'];
		     $user = (new verifyTockenHealper())->verifyToken($token);


		     Log::info('ApiUserLoginController@getMachineofStage user '.print_r($user,true));
		    // Log::info('ApiUserLoginController@getAllStageProduction plant '.print_r($user->plant_id,true));
		     if(!$user){

		     	   $responseObj->status_code = 1001;
                   $responseObj->status_msg = 'Login Fail';
                   $responseObj->data = [];



		     }else{


		     	  $machine=DB::table('tbl_machine_master')
		     	       

		     	  ->where('tbl_machine_master.final_stage',$stage_id)
		     	   ->where('tbl_machine_master.plant_id',$plant_id)
		     	  ->select('tbl_machine_master.*')
		     	  ->get();

		     	  $responseObj->status_code = 1000;
                  $responseObj->status_msg = 'Success';
                  $responseObj->data = $machine;

		     }

		   return Response::json($responseObj , 200);    


 	}



}
