<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeMaster extends Model
{
    protected $table = 'tbl_type_master';
}
