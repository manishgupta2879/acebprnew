<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantStageMaster extends Model
{
   protected $table='tbl_production_stages';

   protected $fillable=['plant_id','stage','is_active','order_no'];

  
}
