<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPlantStageAllocation extends Model
{
     protected $table='tbl_user_stages_allocation';

     protected $fillable=['user_id','stage_id','allocation_date'];
}
