<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantMaster extends Model
{
	protected $table='tbl_plant_master';

    protected $fillable=['plant_name','plant_code','IS_ACTIVE'];

    public function plantName(){
    return $this->belongTo(UserPlantAllocation::class, 'plant_id', 'id');
    }
}
