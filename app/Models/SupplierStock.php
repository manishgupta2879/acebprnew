<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierStock extends Model
{
        protected $table = 'tbl_supplier_stock';
      public $timestamps = false;

	public function loginid(){
		return $this->belongsTo(\App\User::class,'login_id','login_id');
	} 

 }
