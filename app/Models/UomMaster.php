<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UomMaster extends Model
{
    protected $table = 'tbl_uom_master';

    public $timestamps = false;
}
