<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

         Gate::define('ro-admin', function ($user) {
              return $user->user_type ==3;
         });
         
         Gate::define('custermer', function ($user) {
              return $user->user_type ==4;
         });

         Gate::define('administrator', function ($user) {
              return $user->user_type ==1;
         });

         Gate::define('admin', function ($user) {
              return $user->user_type ==2;
         });

        //
    }
}
