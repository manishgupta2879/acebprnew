<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
class User extends Authenticatable
{
    use Notifiable;

 /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    
    protected $table = 'tbl_user_master';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   /* protected $hidden = [
        'password', 'user_token',
    ];
  */


    public function getRememberToken()
        {
            return $this->user_token;
        }

        public function setRememberToken($value)
        {
            $this->user_token = $value;
        }

        public function getRememberTokenName()
        {
            return 'user_token';
        }

     public function customer(){

        return $this->hasOne(RoCustomertManagement::class,'Email','email');


     }

    public function getRocode(){

        return $this->hasOne(RoMaster::class,'Email','email');


     }

     public function getPersonel(){

        return $this->hasOne(RoPersonalManagement::class,'Email','email');


     }

     
}
